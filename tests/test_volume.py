from muscad import Volume, Text, E


def test_chamfer():
    cube = (
        Volume(width=50, depth=50, height=50)
        .reverse_fillet_left()
        .reverse_fillet_right()
        .reverse_fillet_back()
        .reverse_fillet_front()
        .reverse_fillet_bottom()
        .reverse_fillet_top()
    )
    cube -= Text("top", halign="center", valign="center").z_linear_extrude(
        1, top=cube.top + E
    )
    cube -= Text("bottom", halign="center", valign="center").z_linear_extrude(
        1, bottom=cube.bottom - E, downwards=True
    )
    cube -= Text("right", halign="center", valign="center").x_linear_extrude(
        1, right=cube.right + E
    )
    cube -= Text("left", halign="center", valign="center").x_linear_extrude(
        1, left=cube.left - E, leftwards=True
    )
    cube -= Text("front", halign="center", valign="center").y_linear_extrude(
        1, front=cube.front + E
    )
    cube -= Text("back", halign="center", valign="center").y_linear_extrude(
        1, back=cube.back - E, backwards=True
    )

    cube.render_to_file(openscad=False)
