difference() {
  union() {
    // side_bolt_holder
    difference() {
      // volume
      translate(v=[-3.0, 0.0, -8.1]) 
      cube(size=[6.0, 20.0, 16.2], center=true);
      // front_bottom_chamfer
      translate(v=[-6.02, 8.0, -14.2]) 
      rotate(a=[0, 90, 0]) 
      linear_extrude(height=6.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
      // back_bottom_chamfer
      translate(v=[-6.02, -8.0, -14.2]) 
      rotate(a=[270, 0, 0]) 
      rotate(a=[0, 90, 0]) 
      linear_extrude(height=6.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
    }
    // base
    hull() 
    {
      difference() {
        // volume
        translate(v=[2.6, 0.0, 3.0]) 
        cube(size=[27.2, 20.0, 6.0], center=true);
        // front_right_chamfer
        translate(v=[14.2, 8.0, -0.02]) 
        linear_extrude(height=6.04, center=false, convexity=10, twist=0.0, scale=1.0) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          square(size=[2.04, 2.04], center=true);
          // fillet
          circle(d=4, $fn=31);
        }
        // back_right_chamfer
        translate(v=[14.2, -8.0, -0.02]) 
        rotate(a=[0, 0, 270]) 
        linear_extrude(height=6.04, center=false, convexity=10, twist=0.0, scale=1.0) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          square(size=[2.04, 2.04], center=true);
          // fillet
          circle(d=4, $fn=31);
        }
        // back_left_chamfer
        translate(v=[-6.0, -5.0, -0.02]) 
        rotate(a=[0, 0, 180]) 
        linear_extrude(height=6.04, center=false, convexity=10, twist=0.0, scale=1.0) 
        difference() {
          // box
          translate(v=[2.5, 2.5, 0.0]) 
          square(size=[5.04, 5.04], center=true);
          // fillet
          circle(d=10, $fn=78);
        }
        // front_left_chamfer
        translate(v=[-6.0, 5.0, -0.02]) 
        rotate(a=[0, 0, 90]) 
        linear_extrude(height=6.04, center=false, convexity=10, twist=0.0, scale=1.0) 
        difference() {
          // box
          translate(v=[2.5, 2.5, 0.0]) 
          square(size=[5.04, 5.04], center=true);
          // fillet
          circle(d=10, $fn=78);
        }
      }
      linear_extrude(height=6.0, center=false, convexity=10, twist=0.0, scale=1.0) 
      translate(v=[-12.0, 0.0, 0]) 
      circle(d=10, $fn=78);
    }
    // reinforcement
    hull() 
    {
      // base
      hull() 
      {
        difference() {
          // volume
          translate(v=[2.6, 0.0, 3.0]) 
          cube(size=[27.2, 20.0, 6.0], center=true);
          // front_right_chamfer
          translate(v=[14.2, 8.0, -0.02]) 
          linear_extrude(height=6.04, center=false, convexity=10, twist=0.0, scale=1.0) 
          difference() {
            // box
            translate(v=[1.0, 1.0, 0.0]) 
            square(size=[2.04, 2.04], center=true);
            // fillet
            circle(d=4, $fn=31);
          }
          // back_right_chamfer
          translate(v=[14.2, -8.0, -0.02]) 
          rotate(a=[0, 0, 270]) 
          linear_extrude(height=6.04, center=false, convexity=10, twist=0.0, scale=1.0) 
          difference() {
            // box
            translate(v=[1.0, 1.0, 0.0]) 
            square(size=[2.04, 2.04], center=true);
            // fillet
            circle(d=4, $fn=31);
          }
          // back_left_chamfer
          translate(v=[-6.0, -5.0, -0.02]) 
          rotate(a=[0, 0, 180]) 
          linear_extrude(height=6.04, center=false, convexity=10, twist=0.0, scale=1.0) 
          difference() {
            // box
            translate(v=[2.5, 2.5, 0.0]) 
            square(size=[5.04, 5.04], center=true);
            // fillet
            circle(d=10, $fn=78);
          }
          // front_left_chamfer
          translate(v=[-6.0, 5.0, -0.02]) 
          rotate(a=[0, 0, 90]) 
          linear_extrude(height=6.04, center=false, convexity=10, twist=0.0, scale=1.0) 
          difference() {
            // box
            translate(v=[2.5, 2.5, 0.0]) 
            square(size=[5.04, 5.04], center=true);
            // fillet
            circle(d=10, $fn=78);
          }
        }
        linear_extrude(height=6.0, center=false, convexity=10, twist=0.0, scale=1.0) 
        translate(v=[-12.0, 0.0, 0]) 
        circle(d=10, $fn=78);
      }
      // side_bolt_holder
      difference() {
        // volume
        translate(v=[-3.0, 0.0, -8.1]) 
        cube(size=[6.0, 20.0, 16.2], center=true);
        // front_bottom_chamfer
        translate(v=[-6.02, 8.0, -14.2]) 
        rotate(a=[0, 90, 0]) 
        linear_extrude(height=6.04, center=false, convexity=10, twist=0.0, scale=1.0) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          square(size=[2.04, 2.04], center=true);
          // fillet
          circle(d=4, $fn=31);
        }
        // back_bottom_chamfer
        translate(v=[-6.02, -8.0, -14.2]) 
        rotate(a=[270, 0, 0]) 
        rotate(a=[0, 90, 0]) 
        linear_extrude(height=6.04, center=false, convexity=10, twist=0.0, scale=1.0) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          square(size=[2.04, 2.04], center=true);
          // fillet
          circle(d=4, $fn=31);
        }
      }
    }
  }
  // extrusion
  #translate(v=[10.1, 30.0, -10.1]) 
  rotate(a=[270, 0, 180]) 
  // profile
  linear_extrude(height=60, center=false, convexity=10, twist=0.0, scale=1.0) 
  hull() 
  {
    translate(v=[-8.1, -8.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[-8.1, 8.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[8.1, -8.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[8.1, 8.1, 0]) 
    circle(d=4, $fn=31);
  }
  // top_bolt
  translate(v=[10.1, 0.0, -1.19]) 
  rotate(a=[180, 0, 0]) 
  union() {
    hull() 
    {
      // thread
      cylinder(h=10, d=6.374, $fn=50, center=true);
      translate(v=[10, 0, 0]) 
      // thread
      cylinder(h=10, d=6.374, $fn=50, center=true);
    }
    hull() 
    {
      translate(v=[0, 0, -8.18]) 
      cylinder(h=6.4, d=11.9, $fn=93, center=true);
      translate(v=[10, 0, -8.18]) 
      cylinder(h=6.4, d=11.9, $fn=93, center=true);
    }
    hull() 
    {
      translate(v=[0, 0, -61.36]) 
      cylinder(h=100, d=11.9, $fn=93, center=true);
      translate(v=[10, 0, -61.36]) 
      cylinder(h=100, d=11.9, $fn=93, center=true);
    }
  }
  // side_bolt
  translate(v=[-2.0, 0.0, -10.1]) 
  rotate(a=[270, 0, 270]) 
  union() {
    hull() 
    {
      // thread
      cylinder(h=10, d=6.374, $fn=50, center=true);
      translate(v=[0, 10, 0]) 
      // thread
      cylinder(h=10, d=6.374, $fn=50, center=true);
    }
    hull() 
    {
      translate(v=[0, 0, -8.18]) 
      cylinder(h=6.4, d=11.9, $fn=93, center=true);
      translate(v=[0, 10, -8.18]) 
      cylinder(h=6.4, d=11.9, $fn=93, center=true);
    }
    hull() 
    {
      translate(v=[0, 0, -61.36]) 
      cylinder(h=100, d=11.9, $fn=93, center=true);
      translate(v=[0, 10, -61.36]) 
      cylinder(h=100, d=11.9, $fn=93, center=true);
    }
  }
  // clearance
  difference() {
    // volume
    translate(v=[-33.0, 0.0, -10.1]) 
    cube(size=[54.0, 12.0, 20.2], center=true);
    // front_right_chamfer
    translate(v=[-8.0, 4.0, -20.22]) 
    linear_extrude(height=20.24, center=false, convexity=10, twist=0.0, scale=1.0) 
    difference() {
      // box
      translate(v=[1.0, 1.0, 0.0]) 
      square(size=[2.04, 2.04], center=true);
      // fillet
      circle(d=4, $fn=31);
    }
    // back_right_chamfer
    translate(v=[-8.0, -4.0, -20.22]) 
    rotate(a=[0, 0, 270]) 
    linear_extrude(height=20.24, center=false, convexity=10, twist=0.0, scale=1.0) 
    difference() {
      // box
      translate(v=[1.0, 1.0, 0.0]) 
      square(size=[2.04, 2.04], center=true);
      // fillet
      circle(d=4, $fn=31);
    }
  }
  // bed_bolt
  translate(v=[-12.0, 0.0, 18.0]) 
  rotate(a=[0, 180, 0]) 
  union() {
    // thread
    cylinder(h=40, d=3.38, $fn=26, center=true);
    // head
    translate(v=[0, 0, -21.38]) 
    cylinder(h=2.8, d=6.8, $fn=53, center=true);
    // head_clearance
    translate(v=[0, 0, -72.76]) 
    cylinder(h=100, d=6.8, $fn=53, center=true);
    translate(v=[0, 0, 17.7]) 
    // nut
    cylinder(h=2.8, d=6.8, $fn=6, center=true);
  }
}