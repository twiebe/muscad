difference() {
  union() {
    // body
    // volume
    translate(v=[41.25, 10.59, 7.0]) 
    cube(size=[7.6, 17.18, 11.6], center=true);
    // bolt_holder
    hull() 
    difference() {
      // volume
      translate(v=[46.44, 11.28, 7.0]) 
      cube(size=[17.98, 15.8, 11.6], center=true);
      // bottom_left_chamfer
      translate(v=[38.45, 19.2, 2.2]) 
      rotate(a=[90, 180, 0]) 
      linear_extrude(height=15.84, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[0.5, 0.5, 0.0]) 
        square(size=[1.04, 1.04], center=true);
        // fillet
        circle(d=2, $fn=15);
      }
      // top_left_chamfer
      translate(v=[38.45, 19.2, 11.8]) 
      rotate(a=[90, 270, 0]) 
      linear_extrude(height=15.84, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[0.5, 0.5, 0.0]) 
        square(size=[1.04, 1.04], center=true);
        // fillet
        circle(d=2, $fn=15);
      }
      // back_bottom_chamfer
      translate(v=[37.43, 4.38, 2.2]) 
      rotate(a=[270, 0, 0]) 
      rotate(a=[0, 90, 0]) 
      linear_extrude(height=18.02, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[0.5, 0.5, 0.0]) 
        square(size=[1.04, 1.04], center=true);
        // fillet
        circle(d=2, $fn=15);
      }
      // back_top_chamfer
      translate(v=[37.43, 4.38, 11.8]) 
      rotate(a=[180, 0, 0]) 
      rotate(a=[0, 90, 0]) 
      linear_extrude(height=18.02, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[0.5, 0.5, 0.0]) 
        square(size=[1.04, 1.04], center=true);
        // fillet
        circle(d=2, $fn=15);
      }
    }
  }
  // belt
  #translate(v=[41.2955, 28.5379, 0.5]) 
  rotate(a=[0, 0, 90]) 
  union() {
    // tooth
    linear_extrude(height=10, center=false, convexity=10, twist=0.0, scale=1.0) 
    {
      scale(v=[1.1, 1.1, 1.0]) 
      polygon(points=[[0.7472, -0.5], [0.7472, 0], [0.6479, 0.0372], [0.5983, 0.1305], [0.5786, 0.2384], [0.5472, 0.3431], [0.5046, 0.4438], [0.4516, 0.5397], [0.3582, 0.6369], [0.2484, 0.7073], [0.1273, 0.75], [0, 0.7645], [-0.1273, 0.75], [-0.2484, 0.7073], [-0.3582, 0.6369], [-0.4516, 0.5397], [-0.5048, 0.4438], [-0.5473, 0.3431], [-0.5786, 0.2384], [-0.5983, 0.1305], [-0.648, 0.0372], [-0.7472, 0], [-0.7472, -0.5]]);
      translate(v=[-2.032, 0, 0]) 
      scale(v=[1.1, 1.1, 1.0]) 
      polygon(points=[[0.7472, -0.5], [0.7472, 0], [0.6479, 0.0372], [0.5983, 0.1305], [0.5786, 0.2384], [0.5472, 0.3431], [0.5046, 0.4438], [0.4516, 0.5397], [0.3582, 0.6369], [0.2484, 0.7073], [0.1273, 0.75], [0, 0.7645], [-0.1273, 0.75], [-0.2484, 0.7073], [-0.3582, 0.6369], [-0.4516, 0.5397], [-0.5048, 0.4438], [-0.5473, 0.3431], [-0.5786, 0.2384], [-0.5983, 0.1305], [-0.648, 0.0372], [-0.7472, 0], [-0.7472, -0.5]]);
      translate(v=[-4.064, 0, 0]) 
      scale(v=[1.1, 1.1, 1.0]) 
      polygon(points=[[0.7472, -0.5], [0.7472, 0], [0.6479, 0.0372], [0.5983, 0.1305], [0.5786, 0.2384], [0.5472, 0.3431], [0.5046, 0.4438], [0.4516, 0.5397], [0.3582, 0.6369], [0.2484, 0.7073], [0.1273, 0.75], [0, 0.7645], [-0.1273, 0.75], [-0.2484, 0.7073], [-0.3582, 0.6369], [-0.4516, 0.5397], [-0.5048, 0.4438], [-0.5473, 0.3431], [-0.5786, 0.2384], [-0.5983, 0.1305], [-0.648, 0.0372], [-0.7472, 0], [-0.7472, -0.5]]);
      translate(v=[-6.096, 0, 0]) 
      scale(v=[1.1, 1.1, 1.0]) 
      polygon(points=[[0.7472, -0.5], [0.7472, 0], [0.6479, 0.0372], [0.5983, 0.1305], [0.5786, 0.2384], [0.5472, 0.3431], [0.5046, 0.4438], [0.4516, 0.5397], [0.3582, 0.6369], [0.2484, 0.7073], [0.1273, 0.75], [0, 0.7645], [-0.1273, 0.75], [-0.2484, 0.7073], [-0.3582, 0.6369], [-0.4516, 0.5397], [-0.5048, 0.4438], [-0.5473, 0.3431], [-0.5786, 0.2384], [-0.5983, 0.1305], [-0.648, 0.0372], [-0.7472, 0], [-0.7472, -0.5]]);
      translate(v=[-8.128, 0, 0]) 
      scale(v=[1.1, 1.1, 1.0]) 
      polygon(points=[[0.7472, -0.5], [0.7472, 0], [0.6479, 0.0372], [0.5983, 0.1305], [0.5786, 0.2384], [0.5472, 0.3431], [0.5046, 0.4438], [0.4516, 0.5397], [0.3582, 0.6369], [0.2484, 0.7073], [0.1273, 0.75], [0, 0.7645], [-0.1273, 0.75], [-0.2484, 0.7073], [-0.3582, 0.6369], [-0.4516, 0.5397], [-0.5048, 0.4438], [-0.5473, 0.3431], [-0.5786, 0.2384], [-0.5983, 0.1305], [-0.648, 0.0372], [-0.7472, 0], [-0.7472, -0.5]]);
      translate(v=[-10.16, 0, 0]) 
      scale(v=[1.1, 1.1, 1.0]) 
      polygon(points=[[0.7472, -0.5], [0.7472, 0], [0.6479, 0.0372], [0.5983, 0.1305], [0.5786, 0.2384], [0.5472, 0.3431], [0.5046, 0.4438], [0.4516, 0.5397], [0.3582, 0.6369], [0.2484, 0.7073], [0.1273, 0.75], [0, 0.7645], [-0.1273, 0.75], [-0.2484, 0.7073], [-0.3582, 0.6369], [-0.4516, 0.5397], [-0.5048, 0.4438], [-0.5473, 0.3431], [-0.5786, 0.2384], [-0.5983, 0.1305], [-0.648, 0.0372], [-0.7472, 0], [-0.7472, -0.5]]);
      translate(v=[-12.192, 0, 0]) 
      scale(v=[1.1, 1.1, 1.0]) 
      polygon(points=[[0.7472, -0.5], [0.7472, 0], [0.6479, 0.0372], [0.5983, 0.1305], [0.5786, 0.2384], [0.5472, 0.3431], [0.5046, 0.4438], [0.4516, 0.5397], [0.3582, 0.6369], [0.2484, 0.7073], [0.1273, 0.75], [0, 0.7645], [-0.1273, 0.75], [-0.2484, 0.7073], [-0.3582, 0.6369], [-0.4516, 0.5397], [-0.5048, 0.4438], [-0.5473, 0.3431], [-0.5786, 0.2384], [-0.5983, 0.1305], [-0.648, 0.0372], [-0.7472, 0], [-0.7472, -0.5]]);
      translate(v=[-14.224, 0, 0]) 
      scale(v=[1.1, 1.1, 1.0]) 
      polygon(points=[[0.7472, -0.5], [0.7472, 0], [0.6479, 0.0372], [0.5983, 0.1305], [0.5786, 0.2384], [0.5472, 0.3431], [0.5046, 0.4438], [0.4516, 0.5397], [0.3582, 0.6369], [0.2484, 0.7073], [0.1273, 0.75], [0, 0.7645], [-0.1273, 0.75], [-0.2484, 0.7073], [-0.3582, 0.6369], [-0.4516, 0.5397], [-0.5048, 0.4438], [-0.5473, 0.3431], [-0.5786, 0.2384], [-0.5983, 0.1305], [-0.648, 0.0372], [-0.7472, 0], [-0.7472, -0.5]]);
      translate(v=[-16.256, 0, 0]) 
      scale(v=[1.1, 1.1, 1.0]) 
      polygon(points=[[0.7472, -0.5], [0.7472, 0], [0.6479, 0.0372], [0.5983, 0.1305], [0.5786, 0.2384], [0.5472, 0.3431], [0.5046, 0.4438], [0.4516, 0.5397], [0.3582, 0.6369], [0.2484, 0.7073], [0.1273, 0.75], [0, 0.7645], [-0.1273, 0.75], [-0.2484, 0.7073], [-0.3582, 0.6369], [-0.4516, 0.5397], [-0.5048, 0.4438], [-0.5473, 0.3431], [-0.5786, 0.2384], [-0.5983, 0.1305], [-0.648, 0.0372], [-0.7472, 0], [-0.7472, -0.5]]);
      translate(v=[-18.288, 0, 0]) 
      scale(v=[1.1, 1.1, 1.0]) 
      polygon(points=[[0.7472, -0.5], [0.7472, 0], [0.6479, 0.0372], [0.5983, 0.1305], [0.5786, 0.2384], [0.5472, 0.3431], [0.5046, 0.4438], [0.4516, 0.5397], [0.3582, 0.6369], [0.2484, 0.7073], [0.1273, 0.75], [0, 0.7645], [-0.1273, 0.75], [-0.2484, 0.7073], [-0.3582, 0.6369], [-0.4516, 0.5397], [-0.5048, 0.4438], [-0.5473, 0.3431], [-0.5786, 0.2384], [-0.5983, 0.1305], [-0.648, 0.0372], [-0.7472, 0], [-0.7472, -0.5]]);
      translate(v=[-20.32, 0, 0]) 
      scale(v=[1.1, 1.1, 1.0]) 
      polygon(points=[[0.7472, -0.5], [0.7472, 0], [0.6479, 0.0372], [0.5983, 0.1305], [0.5786, 0.2384], [0.5472, 0.3431], [0.5046, 0.4438], [0.4516, 0.5397], [0.3582, 0.6369], [0.2484, 0.7073], [0.1273, 0.75], [0, 0.7645], [-0.1273, 0.75], [-0.2484, 0.7073], [-0.3582, 0.6369], [-0.4516, 0.5397], [-0.5048, 0.4438], [-0.5473, 0.3431], [-0.5786, 0.2384], [-0.5983, 0.1305], [-0.648, 0.0372], [-0.7472, 0], [-0.7472, -0.5]]);
      translate(v=[-22.352, 0, 0]) 
      scale(v=[1.1, 1.1, 1.0]) 
      polygon(points=[[0.7472, -0.5], [0.7472, 0], [0.6479, 0.0372], [0.5983, 0.1305], [0.5786, 0.2384], [0.5472, 0.3431], [0.5046, 0.4438], [0.4516, 0.5397], [0.3582, 0.6369], [0.2484, 0.7073], [0.1273, 0.75], [0, 0.7645], [-0.1273, 0.75], [-0.2484, 0.7073], [-0.3582, 0.6369], [-0.4516, 0.5397], [-0.5048, 0.4438], [-0.5473, 0.3431], [-0.5786, 0.2384], [-0.5983, 0.1305], [-0.648, 0.0372], [-0.7472, 0], [-0.7472, -0.5]]);
      translate(v=[-24.384, 0, 0]) 
      scale(v=[1.1, 1.1, 1.0]) 
      polygon(points=[[0.7472, -0.5], [0.7472, 0], [0.6479, 0.0372], [0.5983, 0.1305], [0.5786, 0.2384], [0.5472, 0.3431], [0.5046, 0.4438], [0.4516, 0.5397], [0.3582, 0.6369], [0.2484, 0.7073], [0.1273, 0.75], [0, 0.7645], [-0.1273, 0.75], [-0.2484, 0.7073], [-0.3582, 0.6369], [-0.4516, 0.5397], [-0.5048, 0.4438], [-0.5473, 0.3431], [-0.5786, 0.2384], [-0.5983, 0.1305], [-0.648, 0.0372], [-0.7472, 0], [-0.7472, -0.5]]);
      translate(v=[-26.416, 0, 0]) 
      scale(v=[1.1, 1.1, 1.0]) 
      polygon(points=[[0.7472, -0.5], [0.7472, 0], [0.6479, 0.0372], [0.5983, 0.1305], [0.5786, 0.2384], [0.5472, 0.3431], [0.5046, 0.4438], [0.4516, 0.5397], [0.3582, 0.6369], [0.2484, 0.7073], [0.1273, 0.75], [0, 0.7645], [-0.1273, 0.75], [-0.2484, 0.7073], [-0.3582, 0.6369], [-0.4516, 0.5397], [-0.5048, 0.4438], [-0.5473, 0.3431], [-0.5786, 0.2384], [-0.5983, 0.1305], [-0.648, 0.0372], [-0.7472, 0], [-0.7472, -0.5]]);
      translate(v=[-28.448, 0, 0]) 
      scale(v=[1.1, 1.1, 1.0]) 
      polygon(points=[[0.7472, -0.5], [0.7472, 0], [0.6479, 0.0372], [0.5983, 0.1305], [0.5786, 0.2384], [0.5472, 0.3431], [0.5046, 0.4438], [0.4516, 0.5397], [0.3582, 0.6369], [0.2484, 0.7073], [0.1273, 0.75], [0, 0.7645], [-0.1273, 0.75], [-0.2484, 0.7073], [-0.3582, 0.6369], [-0.4516, 0.5397], [-0.5048, 0.4438], [-0.5473, 0.3431], [-0.5786, 0.2384], [-0.5983, 0.1305], [-0.648, 0.0372], [-0.7472, 0], [-0.7472, -0.5]]);
      translate(v=[-30.48, 0, 0]) 
      scale(v=[1.1, 1.1, 1.0]) 
      polygon(points=[[0.7472, -0.5], [0.7472, 0], [0.6479, 0.0372], [0.5983, 0.1305], [0.5786, 0.2384], [0.5472, 0.3431], [0.5046, 0.4438], [0.4516, 0.5397], [0.3582, 0.6369], [0.2484, 0.7073], [0.1273, 0.75], [0, 0.7645], [-0.1273, 0.75], [-0.2484, 0.7073], [-0.3582, 0.6369], [-0.4516, 0.5397], [-0.5048, 0.4438], [-0.5473, 0.3431], [-0.5786, 0.2384], [-0.5983, 0.1305], [-0.648, 0.0372], [-0.7472, 0], [-0.7472, -0.5]]);
      translate(v=[-32.512, 0, 0]) 
      scale(v=[1.1, 1.1, 1.0]) 
      polygon(points=[[0.7472, -0.5], [0.7472, 0], [0.6479, 0.0372], [0.5983, 0.1305], [0.5786, 0.2384], [0.5472, 0.3431], [0.5046, 0.4438], [0.4516, 0.5397], [0.3582, 0.6369], [0.2484, 0.7073], [0.1273, 0.75], [0, 0.7645], [-0.1273, 0.75], [-0.2484, 0.7073], [-0.3582, 0.6369], [-0.4516, 0.5397], [-0.5048, 0.4438], [-0.5473, 0.3431], [-0.5786, 0.2384], [-0.5983, 0.1305], [-0.648, 0.0372], [-0.7472, 0], [-0.7472, -0.5]]);
      translate(v=[-34.544, 0, 0]) 
      scale(v=[1.1, 1.1, 1.0]) 
      polygon(points=[[0.7472, -0.5], [0.7472, 0], [0.6479, 0.0372], [0.5983, 0.1305], [0.5786, 0.2384], [0.5472, 0.3431], [0.5046, 0.4438], [0.4516, 0.5397], [0.3582, 0.6369], [0.2484, 0.7073], [0.1273, 0.75], [0, 0.7645], [-0.1273, 0.75], [-0.2484, 0.7073], [-0.3582, 0.6369], [-0.4516, 0.5397], [-0.5048, 0.4438], [-0.5473, 0.3431], [-0.5786, 0.2384], [-0.5983, 0.1305], [-0.648, 0.0372], [-0.7472, 0], [-0.7472, -0.5]]);
      translate(v=[-36.576, 0, 0]) 
      scale(v=[1.1, 1.1, 1.0]) 
      polygon(points=[[0.7472, -0.5], [0.7472, 0], [0.6479, 0.0372], [0.5983, 0.1305], [0.5786, 0.2384], [0.5472, 0.3431], [0.5046, 0.4438], [0.4516, 0.5397], [0.3582, 0.6369], [0.2484, 0.7073], [0.1273, 0.75], [0, 0.7645], [-0.1273, 0.75], [-0.2484, 0.7073], [-0.3582, 0.6369], [-0.4516, 0.5397], [-0.5048, 0.4438], [-0.5473, 0.3431], [-0.5786, 0.2384], [-0.5983, 0.1305], [-0.648, 0.0372], [-0.7472, 0], [-0.7472, -0.5]]);
      translate(v=[-38.608, 0, 0]) 
      scale(v=[1.1, 1.1, 1.0]) 
      polygon(points=[[0.7472, -0.5], [0.7472, 0], [0.6479, 0.0372], [0.5983, 0.1305], [0.5786, 0.2384], [0.5472, 0.3431], [0.5046, 0.4438], [0.4516, 0.5397], [0.3582, 0.6369], [0.2484, 0.7073], [0.1273, 0.75], [0, 0.7645], [-0.1273, 0.75], [-0.2484, 0.7073], [-0.3582, 0.6369], [-0.4516, 0.5397], [-0.5048, 0.4438], [-0.5473, 0.3431], [-0.5786, 0.2384], [-0.5983, 0.1305], [-0.648, 0.0372], [-0.7472, 0], [-0.7472, -0.5]]);
      translate(v=[-40.64, 0, 0]) 
      scale(v=[1.1, 1.1, 1.0]) 
      polygon(points=[[0.7472, -0.5], [0.7472, 0], [0.6479, 0.0372], [0.5983, 0.1305], [0.5786, 0.2384], [0.5472, 0.3431], [0.5046, 0.4438], [0.4516, 0.5397], [0.3582, 0.6369], [0.2484, 0.7073], [0.1273, 0.75], [0, 0.7645], [-0.1273, 0.75], [-0.2484, 0.7073], [-0.3582, 0.6369], [-0.4516, 0.5397], [-0.5048, 0.4438], [-0.5473, 0.3431], [-0.5786, 0.2384], [-0.5983, 0.1305], [-0.648, 0.0372], [-0.7472, 0], [-0.7472, -0.5]]);
      translate(v=[-42.672, 0, 0]) 
      scale(v=[1.1, 1.1, 1.0]) 
      polygon(points=[[0.7472, -0.5], [0.7472, 0], [0.6479, 0.0372], [0.5983, 0.1305], [0.5786, 0.2384], [0.5472, 0.3431], [0.5046, 0.4438], [0.4516, 0.5397], [0.3582, 0.6369], [0.2484, 0.7073], [0.1273, 0.75], [0, 0.7645], [-0.1273, 0.75], [-0.2484, 0.7073], [-0.3582, 0.6369], [-0.4516, 0.5397], [-0.5048, 0.4438], [-0.5473, 0.3431], [-0.5786, 0.2384], [-0.5983, 0.1305], [-0.648, 0.0372], [-0.7472, 0], [-0.7472, -0.5]]);
      translate(v=[-44.704, 0, 0]) 
      scale(v=[1.1, 1.1, 1.0]) 
      polygon(points=[[0.7472, -0.5], [0.7472, 0], [0.6479, 0.0372], [0.5983, 0.1305], [0.5786, 0.2384], [0.5472, 0.3431], [0.5046, 0.4438], [0.4516, 0.5397], [0.3582, 0.6369], [0.2484, 0.7073], [0.1273, 0.75], [0, 0.7645], [-0.1273, 0.75], [-0.2484, 0.7073], [-0.3582, 0.6369], [-0.4516, 0.5397], [-0.5048, 0.4438], [-0.5473, 0.3431], [-0.5786, 0.2384], [-0.5983, 0.1305], [-0.648, 0.0372], [-0.7472, 0], [-0.7472, -0.5]]);
      translate(v=[-46.736, 0, 0]) 
      scale(v=[1.1, 1.1, 1.0]) 
      polygon(points=[[0.7472, -0.5], [0.7472, 0], [0.6479, 0.0372], [0.5983, 0.1305], [0.5786, 0.2384], [0.5472, 0.3431], [0.5046, 0.4438], [0.4516, 0.5397], [0.3582, 0.6369], [0.2484, 0.7073], [0.1273, 0.75], [0, 0.7645], [-0.1273, 0.75], [-0.2484, 0.7073], [-0.3582, 0.6369], [-0.4516, 0.5397], [-0.5048, 0.4438], [-0.5473, 0.3431], [-0.5786, 0.2384], [-0.5983, 0.1305], [-0.648, 0.0372], [-0.7472, 0], [-0.7472, -0.5]]);
      translate(v=[-48.768, 0, 0]) 
      scale(v=[1.1, 1.1, 1.0]) 
      polygon(points=[[0.7472, -0.5], [0.7472, 0], [0.6479, 0.0372], [0.5983, 0.1305], [0.5786, 0.2384], [0.5472, 0.3431], [0.5046, 0.4438], [0.4516, 0.5397], [0.3582, 0.6369], [0.2484, 0.7073], [0.1273, 0.75], [0, 0.7645], [-0.1273, 0.75], [-0.2484, 0.7073], [-0.3582, 0.6369], [-0.4516, 0.5397], [-0.5048, 0.4438], [-0.5473, 0.3431], [-0.5786, 0.2384], [-0.5983, 0.1305], [-0.648, 0.0372], [-0.7472, 0], [-0.7472, -0.5]]);
      translate(v=[-50.8, 0, 0]) 
      scale(v=[1.1, 1.1, 1.0]) 
      polygon(points=[[0.7472, -0.5], [0.7472, 0], [0.6479, 0.0372], [0.5983, 0.1305], [0.5786, 0.2384], [0.5472, 0.3431], [0.5046, 0.4438], [0.4516, 0.5397], [0.3582, 0.6369], [0.2484, 0.7073], [0.1273, 0.75], [0, 0.7645], [-0.1273, 0.75], [-0.2484, 0.7073], [-0.3582, 0.6369], [-0.4516, 0.5397], [-0.5048, 0.4438], [-0.5473, 0.3431], [-0.5786, 0.2384], [-0.5983, 0.1305], [-0.648, 0.0372], [-0.7472, 0], [-0.7472, -0.5]]);
      translate(v=[-52.832, 0, 0]) 
      scale(v=[1.1, 1.1, 1.0]) 
      polygon(points=[[0.7472, -0.5], [0.7472, 0], [0.6479, 0.0372], [0.5983, 0.1305], [0.5786, 0.2384], [0.5472, 0.3431], [0.5046, 0.4438], [0.4516, 0.5397], [0.3582, 0.6369], [0.2484, 0.7073], [0.1273, 0.75], [0, 0.7645], [-0.1273, 0.75], [-0.2484, 0.7073], [-0.3582, 0.6369], [-0.4516, 0.5397], [-0.5048, 0.4438], [-0.5473, 0.3431], [-0.5786, 0.2384], [-0.5983, 0.1305], [-0.648, 0.0372], [-0.7472, 0], [-0.7472, -0.5]]);
      translate(v=[-54.864, 0, 0]) 
      scale(v=[1.1, 1.1, 1.0]) 
      polygon(points=[[0.7472, -0.5], [0.7472, 0], [0.6479, 0.0372], [0.5983, 0.1305], [0.5786, 0.2384], [0.5472, 0.3431], [0.5046, 0.4438], [0.4516, 0.5397], [0.3582, 0.6369], [0.2484, 0.7073], [0.1273, 0.75], [0, 0.7645], [-0.1273, 0.75], [-0.2484, 0.7073], [-0.3582, 0.6369], [-0.4516, 0.5397], [-0.5048, 0.4438], [-0.5473, 0.3431], [-0.5786, 0.2384], [-0.5983, 0.1305], [-0.648, 0.0372], [-0.7472, 0], [-0.7472, -0.5]]);
      translate(v=[-56.896, 0, 0]) 
      scale(v=[1.1, 1.1, 1.0]) 
      polygon(points=[[0.7472, -0.5], [0.7472, 0], [0.6479, 0.0372], [0.5983, 0.1305], [0.5786, 0.2384], [0.5472, 0.3431], [0.5046, 0.4438], [0.4516, 0.5397], [0.3582, 0.6369], [0.2484, 0.7073], [0.1273, 0.75], [0, 0.7645], [-0.1273, 0.75], [-0.2484, 0.7073], [-0.3582, 0.6369], [-0.4516, 0.5397], [-0.5048, 0.4438], [-0.5473, 0.3431], [-0.5786, 0.2384], [-0.5983, 0.1305], [-0.648, 0.0372], [-0.7472, 0], [-0.7472, -0.5]]);
    }
    // belt
    // volume
    translate(v=[-28.448, -0.2, 5.0]) 
    cube(size=[58.5398, 1.1, 10.0], center=true);
  }
  // front_x_pulley
  #translate(v=[46.25, 10.0, -6.35]) 
  union() {
    // body
    cylinder(h=10.7, d=18.4, $fn=144, center=true);
    // bolt
    translate(v=[0, 0, 4.39]) 
    union() {
      // thread
      cylinder(h=25, d=3.38, $fn=26, center=true);
      // head
      translate(v=[0, 0, -13.88]) 
      cylinder(h=2.8, d=6.8, $fn=53, center=true);
      // head_clearance
      translate(v=[0, 0, -65.26]) 
      cylinder(h=100, d=6.8, $fn=53, center=true);
      translate(v=[0, 0, 9.2]) 
      // nut
      cylinder(h=2.8, d=6.8, $fn=6, center=true);
      // nut_clearance
      translate(v=[10.0, 0.0, 9.2]) 
      cube(size=[20, 6.089, 2.8], center=true);
    }
    // clearance
    rotate(a=[0, 0, 270]) 
    translate(v=[0.0, 10.0, 0.0]) 
    cube(size=[18.4, 20, 10.7], center=true);
    // clearance
    translate(v=[0.0, 10.0, 0.0]) 
    cube(size=[18.4, 20, 10.7], center=true);
  }
}