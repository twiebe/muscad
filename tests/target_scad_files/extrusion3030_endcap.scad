union() {
  // base
  difference() {
    // volume
    cube(size=[30.0, 30.0, 6.0], center=true);
    // front_right_chamfer
    translate(v=[11.0, 11.0, -3.02]) 
    linear_extrude(height=6.04, center=false, convexity=10, twist=0.0, scale=1.0) 
    difference() {
      // box
      translate(v=[2.0, 2.0, 0.0]) 
      square(size=[4.04, 4.04], center=true);
      // fillet
      circle(d=8, $fn=62);
    }
    // back_right_chamfer
    translate(v=[11.0, -11.0, -3.02]) 
    rotate(a=[0, 0, 270]) 
    linear_extrude(height=6.04, center=false, convexity=10, twist=0.0, scale=1.0) 
    difference() {
      // box
      translate(v=[2.0, 2.0, 0.0]) 
      square(size=[4.04, 4.04], center=true);
      // fillet
      circle(d=8, $fn=62);
    }
    // back_left_chamfer
    translate(v=[-11.0, -11.0, -3.02]) 
    rotate(a=[0, 0, 180]) 
    linear_extrude(height=6.04, center=false, convexity=10, twist=0.0, scale=1.0) 
    difference() {
      // box
      translate(v=[2.0, 2.0, 0.0]) 
      square(size=[4.04, 4.04], center=true);
      // fillet
      circle(d=8, $fn=62);
    }
    // front_left_chamfer
    translate(v=[-11.0, 11.0, -3.02]) 
    rotate(a=[0, 0, 90]) 
    linear_extrude(height=6.04, center=false, convexity=10, twist=0.0, scale=1.0) 
    difference() {
      // box
      translate(v=[2.0, 2.0, 0.0]) 
      square(size=[4.04, 4.04], center=true);
      // fillet
      circle(d=8, $fn=62);
    }
  }
  // x_insert
  translate(v=[27.5, 0.0, 0.0]) 
  rotate(a=[0, 0, 270]) 
  union() {
    // body
    difference() {
      // volume
      cube(size=[7.8, 25.0, 6.0], center=true);
      // bottom_right_chamfer
      translate(v=[3.4, 12.52, -2.5]) 
      rotate(a=[90, 90, 0]) 
      linear_extrude(height=25.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[0.25, 0.25, 0.0]) 
        square(size=[0.54, 0.54], center=true);
        // fillet
        circle(d=1.0, $fn=7);
      }
      // bottom_left_chamfer
      translate(v=[-3.4, 12.52, -2.5]) 
      rotate(a=[90, 180, 0]) 
      linear_extrude(height=25.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[0.25, 0.25, 0.0]) 
        square(size=[0.54, 0.54], center=true);
        // fillet
        circle(d=1.0, $fn=7);
      }
    }
    // wings
    translate(v=[0, -12.5, 0]) 
    rotate(a=[270, 180, 0]) 
    linear_extrude(height=25, center=false, convexity=10, twist=0.0, scale=1.0) 
    hull() 
    {
      translate(v=[6.0, 0.25, 0]) 
      circle(d=1.5, $fn=20);
      translate(v=[-6.0, 0.25, 0]) 
      circle(d=1.5, $fn=20);
      translate(v=[0.0, 2.5, 0]) 
      square(size=[8, 1], center=true);
    }
  }
  // y_insert
  translate(v=[0.0, 27.5, 0.0]) 
  union() {
    // body
    difference() {
      // volume
      cube(size=[7.8, 25.0, 6.0], center=true);
      // bottom_right_chamfer
      translate(v=[3.4, 12.52, -2.5]) 
      rotate(a=[90, 90, 0]) 
      linear_extrude(height=25.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[0.25, 0.25, 0.0]) 
        square(size=[0.54, 0.54], center=true);
        // fillet
        circle(d=1.0, $fn=7);
      }
      // bottom_left_chamfer
      translate(v=[-3.4, 12.52, -2.5]) 
      rotate(a=[90, 180, 0]) 
      linear_extrude(height=25.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[0.25, 0.25, 0.0]) 
        square(size=[0.54, 0.54], center=true);
        // fillet
        circle(d=1.0, $fn=7);
      }
    }
    // wings
    translate(v=[0, -12.5, 0]) 
    rotate(a=[270, 180, 0]) 
    linear_extrude(height=25, center=false, convexity=10, twist=0.0, scale=1.0) 
    hull() 
    {
      translate(v=[6.0, 0.25, 0]) 
      circle(d=1.5, $fn=20);
      translate(v=[-6.0, 0.25, 0]) 
      circle(d=1.5, $fn=20);
      translate(v=[0.0, 2.5, 0]) 
      square(size=[8, 1], center=true);
    }
  }
  // x_insert_left
  translate(v=[-12.0, 0.0, 15.5]) 
  rotate(a=[270, 0, 270]) 
  union() {
    // body
    difference() {
      // volume
      cube(size=[7.8, 25.0, 6.0], center=true);
      // bottom_right_chamfer
      translate(v=[3.4, 12.52, -2.5]) 
      rotate(a=[90, 90, 0]) 
      linear_extrude(height=25.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[0.25, 0.25, 0.0]) 
        square(size=[0.54, 0.54], center=true);
        // fillet
        circle(d=1.0, $fn=7);
      }
      // bottom_left_chamfer
      translate(v=[-3.4, 12.52, -2.5]) 
      rotate(a=[90, 180, 0]) 
      linear_extrude(height=25.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[0.25, 0.25, 0.0]) 
        square(size=[0.54, 0.54], center=true);
        // fillet
        circle(d=1.0, $fn=7);
      }
    }
    // wings
    translate(v=[0, -12.5, 0]) 
    rotate(a=[270, 180, 0]) 
    linear_extrude(height=25, center=false, convexity=10, twist=0.0, scale=1.0) 
    hull() 
    {
      translate(v=[6.0, 0.25, 0]) 
      circle(d=1.5, $fn=20);
      translate(v=[-6.0, 0.25, 0]) 
      circle(d=1.5, $fn=20);
      translate(v=[0.0, 2.5, 0]) 
      square(size=[8, 1], center=true);
    }
  }
  // z_insert_back
  translate(v=[0.0, -12.0, 15.5]) 
  rotate(a=[270, 0, 0]) 
  union() {
    // body
    difference() {
      // volume
      cube(size=[7.8, 25.0, 6.0], center=true);
      // bottom_right_chamfer
      translate(v=[3.4, 12.52, -2.5]) 
      rotate(a=[90, 90, 0]) 
      linear_extrude(height=25.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[0.25, 0.25, 0.0]) 
        square(size=[0.54, 0.54], center=true);
        // fillet
        circle(d=1.0, $fn=7);
      }
      // bottom_left_chamfer
      translate(v=[-3.4, 12.52, -2.5]) 
      rotate(a=[90, 180, 0]) 
      linear_extrude(height=25.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[0.25, 0.25, 0.0]) 
        square(size=[0.54, 0.54], center=true);
        // fillet
        circle(d=1.0, $fn=7);
      }
    }
    // wings
    translate(v=[0, -12.5, 0]) 
    rotate(a=[270, 180, 0]) 
    linear_extrude(height=25, center=false, convexity=10, twist=0.0, scale=1.0) 
    hull() 
    {
      translate(v=[6.0, 0.25, 0]) 
      circle(d=1.5, $fn=20);
      translate(v=[-6.0, 0.25, 0]) 
      circle(d=1.5, $fn=20);
      translate(v=[0.0, 2.5, 0]) 
      square(size=[8, 1], center=true);
    }
  }
}