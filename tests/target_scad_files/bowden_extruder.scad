difference() {
  union() {
    // drive_gear
    #translate(v=[-37.15, -84.5, 2.15]) 
    rotate(a=[270, 0, 180]) 
    cylinder(h=11, d=12.6, $fn=98, center=true);
    // plate
    difference() {
      // volume
      translate(v=[-42.15, -77.5, 2.15]) 
      cube(size=[52.3, 5.0, 42.3], center=true);
      // top_right_chamfer
      translate(v=[-20.0, -77.5, 19.3]) 
      rotate(a=[90, 0, 0]) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        cube(size=[4.04, 4.04, 5.04], center=true);
        // fillet
        cylinder(h=7.04, d=8, $fn=62, center=true);
      }
      // bottom_right_chamfer
      translate(v=[-20.0, -77.5, -15.0]) 
      rotate(a=[90, 90, 0]) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        cube(size=[4.04, 4.04, 5.04], center=true);
        // fillet
        cylinder(h=7.04, d=8, $fn=62, center=true);
      }
      // bottom_left_chamfer
      translate(v=[-64.3, -77.5, -15.0]) 
      rotate(a=[90, 180, 0]) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        cube(size=[4.04, 4.04, 5.04], center=true);
        // fillet
        cylinder(h=7.04, d=8, $fn=62, center=true);
      }
      // top_left_chamfer
      translate(v=[-64.3, -77.5, 19.3]) 
      rotate(a=[90, 270, 0]) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        cube(size=[4.04, 4.04, 5.04], center=true);
        // fillet
        cylinder(h=7.04, d=8, $fn=62, center=true);
      }
    }
    // bearing_fix
    translate(v=[-55.65, -84.95, 2.15]) 
    rotate(a=[270, 0, 0]) 
    cylinder(h=9.9, d=8.3, $fn=65, center=true);
    // filament_tunnel
    difference() {
      // volume
      translate(v=[-42.675, -86.175, 2.15]) 
      cube(size=[12.0, 12.35, 42.3], center=true);
      // back_right_chamfer
      translate(v=[-38.675, -90.35, 2.15]) 
      rotate(a=[0, 0, 270]) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        cube(size=[2.04, 2.04, 42.34], center=true);
        // fillet
        cylinder(h=44.34, d=4, $fn=31, center=true);
      }
      // back_left_chamfer
      translate(v=[-46.675, -90.35, 2.15]) 
      rotate(a=[0, 0, 180]) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        cube(size=[2.04, 2.04, 42.34], center=true);
        // fillet
        cylinder(h=44.34, d=4, $fn=31, center=true);
      }
    }
    // tightener
    // volume
    translate(v=[-48.675, -86.175, -12.425]) 
    cube(size=[12.0, 12.35, 13.15], center=true);
  }
  // stepper
  #translate(v=[-37.15, 0, 2.15]) 
  rotate(a=[270, 0, 180]) 
  union() {
    // body
    difference() {
      // volume
      translate(v=[0.0, 0.0, 21.0]) 
      cube(size=[42.3, 42.3, 42], center=true);
      // front_right_chamfer
      translate(v=[17.17, 17.17, 21.0]) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        cube(size=[4, 4, 42.04], center=true);
        // chamfer
        rotate(a=[0, 0, 45]) 
        cube(size=[5.6569, 5.6569, 44.04], center=true);
      }
      // back_right_chamfer
      translate(v=[17.17, -17.17, 21.0]) 
      rotate(a=[0, 0, 270]) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        cube(size=[4, 4, 42.04], center=true);
        // chamfer
        rotate(a=[0, 0, 45]) 
        cube(size=[5.6569, 5.6569, 44.04], center=true);
      }
      // back_left_chamfer
      translate(v=[-17.17, -17.17, 21.0]) 
      rotate(a=[0, 0, 180]) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        cube(size=[4, 4, 42.04], center=true);
        // chamfer
        rotate(a=[0, 0, 45]) 
        cube(size=[5.6569, 5.6569, 44.04], center=true);
      }
      // front_left_chamfer
      translate(v=[-17.17, 17.17, 21.0]) 
      rotate(a=[0, 0, 90]) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        cube(size=[4, 4, 42.04], center=true);
        // chamfer
        rotate(a=[0, 0, 45]) 
        cube(size=[5.6569, 5.6569, 44.04], center=true);
      }
    }
    // gearbox
    translate(v=[0.0, 0.0, 53.99]) 
    cylinder(h=24.02, d=36, $fn=282, center=true);
    // bolts
    translate(v=[0, 0, 66.0]) 
    {
      rotate(a=[0, 0, 45]) 
      translate(v=[14.1421, 0, 0]) 
      rotate(a=[180, 0, 0]) 
      union() {
        // thread
        cylinder(h=10, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -6.38]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -9.76]) 
        cylinder(h=4, d=6.8, $fn=53, center=true);
      }
      rotate(a=[0, 0, 135]) 
      translate(v=[14.1421, 0, 0]) 
      rotate(a=[180, 0, 0]) 
      union() {
        // thread
        cylinder(h=10, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -6.38]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -9.76]) 
        cylinder(h=4, d=6.8, $fn=53, center=true);
      }
      rotate(a=[0, 0, 225]) 
      translate(v=[14.1421, 0, 0]) 
      rotate(a=[180, 0, 0]) 
      union() {
        // thread
        cylinder(h=10, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -6.38]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -9.76]) 
        cylinder(h=4, d=6.8, $fn=53, center=true);
      }
      rotate(a=[0, 0, 315]) 
      translate(v=[14.1421, 0, 0]) 
      rotate(a=[180, 0, 0]) 
      union() {
        // thread
        cylinder(h=10, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -6.38]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -9.76]) 
        cylinder(h=4, d=6.8, $fn=53, center=true);
      }
    }
    // central_bulge
    translate(v=[0.0, 0.0, 67.0]) 
    cylinder(h=4, d=22.4, $fn=175, center=true);
    // shaft
    translate(v=[0.0, 0.0, 79.48]) 
    cylinder(h=27, d=9.4, $fn=73, center=true);
    // bolts
    translate(v=[0, 0, 70.0]) 
    {
      rotate(a=[0, 0, 135]) 
      translate(v=[24.0416, 0, 0]) 
      rotate(a=[180, 0, 0]) 
      union() {
        // thread
        cylinder(h=12, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -7.38]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -58.76]) 
        cylinder(h=100, d=6.8, $fn=53, center=true);
        translate(v=[0, 0, 3.7]) 
        // nut
        cylinder(h=2.8, d=6.8, $fn=6, center=true);
        // nut_clearance
        translate(v=[10.0, 0.0, 3.7]) 
        cube(size=[20, 6.089, 2.8], center=true);
      }
      rotate(a=[0, 0, 225]) 
      translate(v=[24.0416, 0, 0]) 
      rotate(a=[180, 0, 0]) 
      union() {
        // thread
        cylinder(h=12, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -7.38]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -58.76]) 
        cylinder(h=100, d=6.8, $fn=53, center=true);
        translate(v=[0, 0, 3.7]) 
        // nut
        cylinder(h=2.8, d=6.8, $fn=6, center=true);
        // nut_clearance
        translate(v=[10.0, 0.0, 3.7]) 
        cube(size=[20, 6.089, 2.8], center=true);
      }
    }
  }
  // bracket_bolt
  translate(v=[-62.3, -74.98, 17.3]) 
  rotate(a=[270, 0, 0]) 
  union() {
    // thread
    cylinder(h=20, d=4.378, $fn=34, center=true);
    // head
    translate(v=[0, 0, -11.78]) 
    cylinder(h=3.6, d=8.5, $fn=66, center=true);
    // head_clearance
    translate(v=[0, 0, -23.56]) 
    cylinder(h=20, d=8.5, $fn=66, center=true);
    rotate(a=[0, 0, 90]) 
    translate(v=[0, 0, 8.28]) 
    // nut
    cylinder(h=3.6, d=8.5, $fn=6, center=true);
  }
  // plate_bolt
  translate(v=[-62.3, -70.98, -13.0]) 
  rotate(a=[270, 0, 0]) 
  union() {
    // thread
    cylinder(h=12, d=3.38, $fn=26, center=true);
    // head
    translate(v=[0, 0, -7.38]) 
    cylinder(h=2.8, d=6.8, $fn=53, center=true);
    // head_clearance
    translate(v=[0, 0, -58.76]) 
    cylinder(h=100, d=6.8, $fn=53, center=true);
    rotate(a=[0, 0, 90]) 
    translate(v=[0, 0, 4.68]) 
    // nut
    cylinder(h=2.8, d=6.8, $fn=6, center=true);
  }
  // central_hole
  translate(v=[-37.15, -85.02, 2.15]) 
  rotate(a=[270, 0, 180]) 
  cylinder(h=20, d=16, $fn=125, center=true);
  // filament
  #translate(v=[-43.825, -87.5, 2.15]) 
  cylinder(h=44.3, d=4, $fn=31, center=true);
  // bearing
  #translate(v=[-55.65, -86.3, 2.15]) 
  rotate(a=[270, 0, 0]) 
  difference() {
    // outer
    cylinder(h=7.4, d=22.4, $fn=175, center=true);
    // inner
    cylinder(h=7.44, d=8.4, $fn=65, center=true);
  }
  // bearing_bolt
  #translate(v=[-55.65, -82.62, 2.15]) 
  rotate(a=[270, 0, 0]) 
  union() {
    // thread
    cylinder(h=16, d=5.376, $fn=42, center=true);
    // head
    translate(v=[0, 0, -10.18]) 
    cylinder(h=4.4, d=9.6, $fn=75, center=true);
    // head_clearance
    translate(v=[0, 0, -62.36]) 
    cylinder(h=100, d=9.6, $fn=75, center=true);
  }
  // bearing_clearance
  difference() {
    translate(v=[-55.65, -95.4, 2.15]) 
    rotate(a=[270, 0, 0]) 
    cylinder(h=25.4, d=25.9, $fn=203, center=true);
    // bearing_fix
    translate(v=[-55.65, -84.95, 2.15]) 
    rotate(a=[270, 0, 0]) 
    cylinder(h=9.9, d=8.3, $fn=65, center=true);
  }
  // pneumatic
  translate(v=[-43.825, -87.5, 20.82]) 
  cylinder(h=5, d=5.7, $fn=44, center=true);
  // central_clearance
  // volume
  translate(v=[-42.675, -86.185, 2.15]) 
  cube(size=[12.04, 12.37, 16.0], center=true);
  // bracket_tightening_bolt
  #translate(v=[-55.825, -86.175, -15.5]) 
  union() {
    hull() 
    {
      rotate(a=[0, 90, 0]) 
      // thread
      cylinder(h=16, d=3.38, $fn=26, center=true);
      translate(v=[0, 0, 2]) 
      rotate(a=[0, 90, 0]) 
      // thread
      cylinder(h=16, d=3.38, $fn=26, center=true);
    }
    hull() 
    {
      rotate(a=[0, 90, 0]) 
      translate(v=[0, 0, -9.38]) 
      cylinder(h=2.8, d=6.8, $fn=53, center=true);
      translate(v=[0, 0, 2]) 
      rotate(a=[0, 90, 0]) 
      translate(v=[0, 0, -9.38]) 
      cylinder(h=2.8, d=6.8, $fn=53, center=true);
    }
    hull() 
    {
      rotate(a=[0, 90, 0]) 
      translate(v=[0, 0, -60.76]) 
      cylinder(h=100, d=6.8, $fn=53, center=true);
      translate(v=[0, 0, 2]) 
      rotate(a=[0, 90, 0]) 
      translate(v=[0, 0, -60.76]) 
      cylinder(h=100, d=6.8, $fn=53, center=true);
    }
    hull() 
    {
      rotate(a=[0, 90, 0]) 
      rotate(a=[0, 0, 180]) 
      translate(v=[0, 0, 4.7]) 
      // nut
      cylinder(h=2.8, d=6.8, $fn=6, center=true);
      translate(v=[0, 0, 2]) 
      rotate(a=[0, 90, 0]) 
      rotate(a=[0, 0, 180]) 
      translate(v=[0, 0, 4.7]) 
      // nut
      cylinder(h=2.8, d=6.8, $fn=6, center=true);
    }
    hull() 
    {
      rotate(a=[0, 90, 0]) 
      rotate(a=[0, 0, 180]) 
      translate(v=[4.0, 0.0, 4.7]) 
      cube(size=[8, 6.089, 2.8], center=true);
      translate(v=[0, 0, 2]) 
      rotate(a=[0, 90, 0]) 
      rotate(a=[0, 0, 180]) 
      translate(v=[4.0, 0.0, 4.7]) 
      cube(size=[8, 6.089, 2.8], center=true);
    }
  }
}