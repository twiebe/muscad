difference() {
  union() {
    // clamp
    difference() {
      // volume
      translate(v=[0.0, 52.87, 0.0]) 
      cube(size=[40.0, 12.4, 11.0], center=true);
      // bottom_right_chamfer
      translate(v=[18.0, 59.09, -3.5]) 
      rotate(a=[90, 90, 0]) 
      linear_extrude(height=12.44, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
      // bottom_left_chamfer
      translate(v=[-18.0, 59.09, -3.5]) 
      rotate(a=[90, 180, 0]) 
      linear_extrude(height=12.44, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
      // top_left_chamfer
      translate(v=[-18.0, 59.09, 3.5]) 
      rotate(a=[90, 270, 0]) 
      linear_extrude(height=12.44, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
    }
    // fan_holder
    hull() 
    {
      translate(v=[13.0, 48.67, -17.0]) 
      cylinder(h=34, d=4, $fn=31, center=true);
      translate(v=[-13.0, 48.67, -17.0]) 
      cylinder(h=34, d=4, $fn=31, center=true);
      // volume
      translate(v=[0.0, 58.57, -17.5]) 
      cube(size=[40.0, 1.0, 35.0], center=true);
    }
    // sensor_holder_up
    difference() {
      // volume
      translate(v=[28.98, 50.07, 1.0]) 
      cube(size=[18.0, 18.0, 9.0], center=true);
      // front_right_chamfer
      translate(v=[31.98, 53.07, -3.52]) 
      linear_extrude(height=9.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[3.0, 3.0, 0.0]) 
        square(size=[6.04, 6.04], center=true);
        // fillet
        circle(d=12, $fn=94);
      }
      // back_right_chamfer
      translate(v=[31.98, 47.07, -3.52]) 
      rotate(a=[0, 0, 270]) 
      linear_extrude(height=9.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[3.0, 3.0, 0.0]) 
        square(size=[6.04, 6.04], center=true);
        // fillet
        circle(d=12, $fn=94);
      }
      // back_left_chamfer
      translate(v=[25.98, 47.07, -3.52]) 
      rotate(a=[0, 0, 180]) 
      linear_extrude(height=9.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[3.0, 3.0, 0.0]) 
        square(size=[6.04, 6.04], center=true);
        // fillet
        circle(d=12, $fn=94);
      }
    }
    // sensor_holder_down
    translate(v=[0, 0, -14.75]) 
    mirror(v=[0, 0, 1]) 
    translate(v=[0, 0, 14.75]) 
    // sensor_holder_up
    difference() {
      // volume
      translate(v=[28.98, 50.07, 1.0]) 
      cube(size=[18.0, 18.0, 9.0], center=true);
      // front_right_chamfer
      translate(v=[31.98, 53.07, -3.52]) 
      linear_extrude(height=9.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[3.0, 3.0, 0.0]) 
        square(size=[6.04, 6.04], center=true);
        // fillet
        circle(d=12, $fn=94);
      }
      // back_right_chamfer
      translate(v=[31.98, 47.07, -3.52]) 
      rotate(a=[0, 0, 270]) 
      linear_extrude(height=9.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[3.0, 3.0, 0.0]) 
        square(size=[6.04, 6.04], center=true);
        // fillet
        circle(d=12, $fn=94);
      }
      // back_left_chamfer
      translate(v=[25.98, 47.07, -3.52]) 
      rotate(a=[0, 0, 180]) 
      linear_extrude(height=9.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[3.0, 3.0, 0.0]) 
        square(size=[6.04, 6.04], center=true);
        // fillet
        circle(d=12, $fn=94);
      }
    }
    // sensor_holder_down_holder
    difference() {
      // volume
      translate(v=[0.0, 52.87, -30.5]) 
      cube(size=[40.0, 12.4, 9.0], center=true);
      // back_right_chamfer
      translate(v=[18.0, 48.67, -35.02]) 
      rotate(a=[0, 0, 270]) 
      linear_extrude(height=9.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
      // back_left_chamfer
      translate(v=[-18.0, 48.67, -35.02]) 
      rotate(a=[0, 0, 180]) 
      linear_extrude(height=9.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
    }
  }
  // extruder
  #translate(v=[0.0, 46.995, -54.31]) 
  // extruder
  union() {
    translate(v=[0, 0, 0.99]) 
    cylinder(h=2, d1=1, d2=6, $fn=7, center=true);
    translate(v=[0, 0, 2.98]) 
    cylinder(h=2, d=8.06, $fn=6, center=true);
    translate(v=[0, 0, 4.97]) 
    cylinder(h=2, d=5, $fn=39, center=true);
    translate(v=[0, 0, 5.96]) 
    // volume
    translate(v=[0.0, 5.5, 5.25]) 
    cube(size=[16, 20.0, 10.5], center=true);
    translate(v=[0, 0, 18.0]) 
    cylinder(h=3.1, d=4, $fn=31, center=true);
    translate(v=[0, 0, 32.04]) 
    cylinder(h=25, d=22.3, $fn=175, center=true);
    translate(v=[0, 0, 48.03]) 
    cylinder(h=7, d=16, $fn=125, center=true);
    translate(v=[0, 0, 54.42]) 
    cylinder(h=5.8, d=12, $fn=94, center=true);
    translate(v=[0, 0, 60.81]) 
    cylinder(h=7, d=16, $fn=125, center=true);
  }
  // extruder_clamp_bolt_left
  mirror(v=[1, 0, 0]) 
  // extruder_clamp_bolt_right
  translate(v=[-16.0, 44.78, 1.3]) 
  rotate(a=[90, 0, 0]) 
  union() {
    // thread
    cylinder(h=12, d=3.38, $fn=26, center=true);
    // head
    translate(v=[0, 0, -7.38]) 
    cylinder(h=2.8, d=6.8, $fn=53, center=true);
    // head_clearance
    translate(v=[0, 0, -58.76]) 
    cylinder(h=100, d=6.8, $fn=53, center=true);
    rotate(a=[0, 0, 90]) 
    translate(v=[0, 0, 4.68]) 
    // nut
    cylinder(h=2.8, d=6.8, $fn=6, center=true);
    // nut_clearance
    rotate(a=[0, 0, 90]) 
    translate(v=[10.0, 0.0, 4.68]) 
    cube(size=[20, 6.089, 2.8], center=true);
  }
  // extruder_clamp_bolt_right
  translate(v=[-16.0, 44.78, 1.3]) 
  rotate(a=[90, 0, 0]) 
  union() {
    // thread
    cylinder(h=12, d=3.38, $fn=26, center=true);
    // head
    translate(v=[0, 0, -7.38]) 
    cylinder(h=2.8, d=6.8, $fn=53, center=true);
    // head_clearance
    translate(v=[0, 0, -58.76]) 
    cylinder(h=100, d=6.8, $fn=53, center=true);
    rotate(a=[0, 0, 90]) 
    translate(v=[0, 0, 4.68]) 
    // nut
    cylinder(h=2.8, d=6.8, $fn=6, center=true);
    // nut_clearance
    rotate(a=[0, 0, 90]) 
    translate(v=[10.0, 0.0, 4.68]) 
    cube(size=[20, 6.089, 2.8], center=true);
  }
  // fan
  translate(v=[0.0, 69.37, -14.7]) 
  rotate(a=[90, 0, 0]) 
  union() {
    // body
    difference() {
      // volume
      cube(size=[40.4, 40.4, 20.4], center=true);
      // front_right_chamfer
      translate(v=[18.2, 18.2, -10.22]) 
      linear_extrude(height=20.44, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
      // back_right_chamfer
      translate(v=[18.2, -18.2, -10.22]) 
      rotate(a=[0, 0, 270]) 
      linear_extrude(height=20.44, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
      // back_left_chamfer
      translate(v=[-18.2, -18.2, -10.22]) 
      rotate(a=[0, 0, 180]) 
      linear_extrude(height=20.44, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
      // front_left_chamfer
      translate(v=[-18.2, 18.2, -10.22]) 
      rotate(a=[0, 0, 90]) 
      linear_extrude(height=20.44, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
    }
    // bolts
    translate(v=[0, 0, 5.08]) 
    {
      rotate(a=[0, 0, 225]) 
      translate(v=[22.6274, 0, 0]) 
      union() {
        // thread
        cylinder(h=25, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -13.88]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -65.26]) 
        cylinder(h=100, d=6.8, $fn=53, center=true);
        translate(v=[0, 0, 11.18]) 
        // nut
        cylinder(h=2.8, d=6.8, $fn=6, center=true);
      }
      rotate(a=[0, 0, 315]) 
      translate(v=[22.6274, 0, 0]) 
      union() {
        // thread
        cylinder(h=25, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -13.88]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -65.26]) 
        cylinder(h=100, d=6.8, $fn=53, center=true);
        translate(v=[0, 0, 11.18]) 
        // nut
        cylinder(h=2.8, d=6.8, $fn=6, center=true);
      }
    }
    // bolts
    translate(v=[0, 0, 12.58]) 
    {
      rotate(a=[0, 0, 45]) 
      translate(v=[22.6274, 0, 0]) 
      union() {
        // thread
        cylinder(h=40, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -21.38]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -72.76]) 
        cylinder(h=100, d=6.8, $fn=53, center=true);
      }
      rotate(a=[0, 0, 135]) 
      translate(v=[22.6274, 0, 0]) 
      union() {
        // thread
        cylinder(h=40, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -21.38]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -72.76]) 
        cylinder(h=100, d=6.8, $fn=53, center=true);
      }
    }
  }
  // tunnel
  hull() 
  {
    difference() {
      // volume
      translate(v=[0.0, 59.57, -20.5]) 
      cube(size=[32.0, 1.0, 34.0], center=true);
      // top_right_chamfer
      translate(v=[8.0, 60.09, -11.5]) 
      rotate(a=[90, 0, 0]) 
      linear_extrude(height=1.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[4.0, 4.0, 0.0]) 
        square(size=[8.04, 8.04], center=true);
        // fillet
        circle(d=16, $fn=125);
      }
      // top_left_chamfer
      translate(v=[-8.0, 60.09, -11.5]) 
      rotate(a=[90, 270, 0]) 
      linear_extrude(height=1.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[4.0, 4.0, 0.0]) 
        square(size=[8.04, 8.04], center=true);
        // fillet
        circle(d=16, $fn=125);
      }
    }
    difference() {
      // volume
      translate(v=[0.0, 46.17, -21.5]) 
      cube(size=[24.0, 1.0, 32.0], center=true);
      // top_right_chamfer
      translate(v=[4.0, 46.69, -13.5]) 
      rotate(a=[90, 0, 0]) 
      linear_extrude(height=1.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[4.0, 4.0, 0.0]) 
        square(size=[8.04, 8.04], center=true);
        // fillet
        circle(d=16, $fn=125);
      }
      // top_left_chamfer
      translate(v=[-4.0, 46.69, -13.5]) 
      rotate(a=[90, 270, 0]) 
      linear_extrude(height=1.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[4.0, 4.0, 0.0]) 
        square(size=[8.04, 8.04], center=true);
        // fillet
        circle(d=16, $fn=125);
      }
    }
  }
  // sensor
  #translate(v=[28.98, 50.07, -20.32]) 
  // sensor
  cylinder(h=60, d=12, $fn=94, center=true);
}