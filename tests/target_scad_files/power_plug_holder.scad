difference() {
  union() {
    // holder
    difference() {
      // volume
      translate(v=[-45.2, 0.0, 55.3]) 
      cube(size=[60.0, 26.2, 50.0], center=true);
      // top_left_chamfer
      translate(v=[-72.2, 13.12, 77.3]) 
      rotate(a=[90, 270, 0]) 
      linear_extrude(height=26.24, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.5, 1.5, 0.0]) 
        square(size=[3.04, 3.04], center=true);
        // fillet
        circle(d=6, $fn=47);
      }
    }
    // fixation_left
    difference() {
      // volume
      translate(v=[-82.72, 0.0, 32.2]) 
      cube(size=[15.0, 26.2, 4.0], center=true);
      // back_left_chamfer
      translate(v=[-81.22, -4.1, 30.18]) 
      rotate(a=[0, 0, 180]) 
      linear_extrude(height=4.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[4.5, 4.5, 0.0]) 
        square(size=[9.04, 9.04], center=true);
        // fillet
        circle(d=18, $fn=141);
      }
      // front_left_chamfer
      translate(v=[-81.22, 4.1, 30.18]) 
      rotate(a=[0, 0, 90]) 
      linear_extrude(height=4.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[4.5, 4.5, 0.0]) 
        square(size=[9.04, 9.04], center=true);
        // fillet
        circle(d=18, $fn=141);
      }
    }
    // fixation_top
    difference() {
      // volume
      translate(v=[-17.1, 0.0, 87.78]) 
      cube(size=[4.0, 26.2, 15.0], center=true);
      // front_top_chamfer
      translate(v=[-19.12, 4.1, 86.28]) 
      rotate(a=[90, 0, 0]) 
      rotate(a=[0, 90, 0]) 
      linear_extrude(height=4.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[4.5, 4.5, 0.0]) 
        square(size=[9.04, 9.04], center=true);
        // fillet
        circle(d=18, $fn=141);
      }
      // back_top_chamfer
      translate(v=[-19.12, -4.1, 86.28]) 
      rotate(a=[180, 0, 0]) 
      rotate(a=[0, 90, 0]) 
      linear_extrude(height=4.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[4.5, 4.5, 0.0]) 
        square(size=[9.04, 9.04], center=true);
        // fillet
        circle(d=18, $fn=141);
      }
    }
  }
  // z_extrusion
  // profile
  #linear_extrude(height=100, center=false, convexity=10, twist=0.0, scale=1.0) 
  hull() 
  {
    translate(v=[-13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[-13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
  }
  // x_extrusion
  #translate(v=[-95.1, 0, 15.1]) 
  rotate(a=[270, 0, 270]) 
  // profile
  linear_extrude(height=80, center=false, convexity=10, twist=0.0, scale=1.0) 
  hull() 
  {
    translate(v=[-13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[-13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
  }
  // hollow
  #difference() {
    // volume
    translate(v=[-45.2, 0.0, 55.3]) 
    cube(size=[48.0, 30.2, 28.0], center=true);
    // top_right_chamfer
    translate(v=[-24.18, 0.0, 66.32]) 
    rotate(a=[90, 0, 0]) 
    difference() {
      // box
      translate(v=[1.5, 1.5, 0.0]) 
      cube(size=[3, 3, 30.24], center=true);
      // chamfer
      rotate(a=[0, 0, 45]) 
      cube(size=[4.2426, 4.2426, 32.24], center=true);
    }
    // bottom_right_chamfer
    translate(v=[-24.18, 0.0, 44.28]) 
    rotate(a=[90, 90, 0]) 
    difference() {
      // box
      translate(v=[1.5, 1.5, 0.0]) 
      cube(size=[3, 3, 30.24], center=true);
      // chamfer
      rotate(a=[0, 0, 45]) 
      cube(size=[4.2426, 4.2426, 32.24], center=true);
    }
    // bottom_left_chamfer
    translate(v=[-68.2, 15.12, 42.3]) 
    rotate(a=[90, 180, 0]) 
    linear_extrude(height=30.24, center=false, convexity=10, twist=0.0, scale=1.0) 
    difference() {
      // box
      translate(v=[0.5, 0.5, 0.0]) 
      square(size=[1.04, 1.04], center=true);
      // fillet
      circle(d=2, $fn=15);
    }
    // top_left_chamfer
    translate(v=[-68.2, 15.12, 68.3]) 
    rotate(a=[90, 270, 0]) 
    linear_extrude(height=30.24, center=false, convexity=10, twist=0.0, scale=1.0) 
    difference() {
      // box
      translate(v=[0.5, 0.5, 0.0]) 
      square(size=[1.04, 1.04], center=true);
      // fillet
      circle(d=2, $fn=15);
    }
  }
  // fixation_bolt_left
  #translate(v=[-83.2, 0.0, 27.01]) 
  rotate(a=[0, 180, 0]) 
  union() {
    // thread
    cylinder(h=10, d=6.374, $fn=50, center=true);
    // head
    translate(v=[0, 0, -8.18]) 
    cylinder(h=6.4, d=11.9, $fn=93, center=true);
    // head_clearance
    translate(v=[0, 0, -61.36]) 
    cylinder(h=100, d=11.9, $fn=93, center=true);
  }
  // fixation_bolt_top
  #translate(v=[-15.1, 0.0, 88.3]) 
  rotate(a=[270, 0, 270]) 
  union() {
    // thread
    cylinder(h=10, d=6.374, $fn=50, center=true);
    // head
    translate(v=[0, 0, -8.18]) 
    cylinder(h=6.4, d=11.9, $fn=93, center=true);
    // head_clearance
    translate(v=[0, 0, -61.36]) 
    cylinder(h=100, d=11.9, $fn=93, center=true);
  }
}