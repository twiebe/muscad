difference() {
  union() {
    // holder
    difference() {
      difference() {
        // volume
        translate(v=[-10.1, 71.1, 86.2]) 
        cube(size=[6.0, 112.0, 112.0], center=true);
        // front_top_chamfer
        translate(v=[-13.12, 125.1, 140.2]) 
        rotate(a=[90, 0, 0]) 
        rotate(a=[0, 90, 0]) 
        linear_extrude(height=6.04, center=false, convexity=10, twist=0.0, scale=1.0) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          square(size=[2.04, 2.04], center=true);
          // fillet
          circle(d=4, $fn=31);
        }
        // front_bottom_chamfer
        translate(v=[-13.12, 125.1, 32.2]) 
        rotate(a=[0, 90, 0]) 
        linear_extrude(height=6.04, center=false, convexity=10, twist=0.0, scale=1.0) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          square(size=[2.04, 2.04], center=true);
          // fillet
          circle(d=4, $fn=31);
        }
        // back_bottom_chamfer
        translate(v=[-13.12, 17.1, 32.2]) 
        rotate(a=[270, 0, 0]) 
        rotate(a=[0, 90, 0]) 
        linear_extrude(height=6.04, center=false, convexity=10, twist=0.0, scale=1.0) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          square(size=[2.04, 2.04], center=true);
          // fillet
          circle(d=4, $fn=31);
        }
        // back_top_chamfer
        translate(v=[-13.12, 17.1, 140.2]) 
        rotate(a=[180, 0, 0]) 
        rotate(a=[0, 90, 0]) 
        linear_extrude(height=6.04, center=false, convexity=10, twist=0.0, scale=1.0) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          square(size=[2.04, 2.04], center=true);
          // fillet
          circle(d=4, $fn=31);
        }
      }
      difference() {
        // volume
        translate(v=[-10.1, 70.1, 90.2]) 
        cube(size=[10.0, 90.0, 80.0], center=true);
        // front_top_chamfer
        translate(v=[-15.12, 113.1, 128.2]) 
        rotate(a=[90, 0, 0]) 
        rotate(a=[0, 90, 0]) 
        linear_extrude(height=10.04, center=false, convexity=10, twist=0.0, scale=1.0) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          square(size=[2.04, 2.04], center=true);
          // fillet
          circle(d=4, $fn=31);
        }
        // front_bottom_chamfer
        translate(v=[-15.12, 113.1, 52.2]) 
        rotate(a=[0, 90, 0]) 
        linear_extrude(height=10.04, center=false, convexity=10, twist=0.0, scale=1.0) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          square(size=[2.04, 2.04], center=true);
          // fillet
          circle(d=4, $fn=31);
        }
        // back_bottom_chamfer
        translate(v=[-15.12, 27.1, 52.2]) 
        rotate(a=[270, 0, 0]) 
        rotate(a=[0, 90, 0]) 
        linear_extrude(height=10.04, center=false, convexity=10, twist=0.0, scale=1.0) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          square(size=[2.04, 2.04], center=true);
          // fillet
          circle(d=4, $fn=31);
        }
        // back_top_chamfer
        translate(v=[-15.12, 27.1, 128.2]) 
        rotate(a=[180, 0, 0]) 
        rotate(a=[0, 90, 0]) 
        linear_extrude(height=10.04, center=false, convexity=10, twist=0.0, scale=1.0) 
        difference() {
          // box
          translate(v=[1.0, 1.0, 0.0]) 
          square(size=[2.04, 2.04], center=true);
          // fillet
          circle(d=4, $fn=31);
        }
      }
    }
    // z_attach
    difference() {
      // volume
      translate(v=[0.0, 18.1, 96.675]) 
      cube(size=[26.0, 6.0, 132.95], center=true);
      // top_right_chamfer
      translate(v=[0.0, 21.12, 150.15]) 
      rotate(a=[90, 0, 0]) 
      linear_extrude(height=6.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[6.5, 6.5, 0.0]) 
        square(size=[13.04, 13.04], center=true);
        // fillet
        circle(d=26, $fn=204);
      }
      // top_left_chamfer
      translate(v=[0.0, 21.12, 150.15]) 
      rotate(a=[90, 270, 0]) 
      linear_extrude(height=6.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[6.5, 6.5, 0.0]) 
        square(size=[13.04, 13.04], center=true);
        // fillet
        circle(d=26, $fn=204);
      }
      // back_bottom_chamfer
      translate(v=[-13.02, 17.1, 32.2]) 
      rotate(a=[270, 0, 0]) 
      rotate(a=[0, 90, 0]) 
      linear_extrude(height=26.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
    }
    // y_attach
    difference() {
      // volume
      translate(v=[0.0, 81.575, 33.2]) 
      cube(size=[26.0, 132.95, 6.0], center=true);
      // front_right_chamfer
      translate(v=[0.0, 135.05, 30.18]) 
      linear_extrude(height=6.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[6.5, 6.5, 0.0]) 
        square(size=[13.04, 13.04], center=true);
        // fillet
        circle(d=26, $fn=204);
      }
      // front_left_chamfer
      translate(v=[0.0, 135.05, 30.18]) 
      rotate(a=[0, 0, 90]) 
      linear_extrude(height=6.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[6.5, 6.5, 0.0]) 
        square(size=[13.04, 13.04], center=true);
        // fillet
        circle(d=26, $fn=204);
      }
      // back_bottom_chamfer
      translate(v=[-13.02, 17.1, 32.2]) 
      rotate(a=[270, 0, 0]) 
      rotate(a=[0, 90, 0]) 
      linear_extrude(height=26.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
    }
  }
  // z_extrusion
  // profile
  #linear_extrude(height=200, center=false, convexity=10, twist=0.0, scale=1.0) 
  hull() 
  {
    translate(v=[-13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[-13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
  }
  // y_extrusion
  #translate(v=[0.0, 215.1, 15.1]) 
  rotate(a=[270, 0, 180]) 
  // profile
  linear_extrude(height=200, center=false, convexity=10, twist=0.0, scale=1.0) 
  hull() 
  {
    translate(v=[-13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[-13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
  }
  // board
  #translate(v=[-4.0, 75.1, 90.2]) 
  rotate(a=[270, 0, 270]) 
  union() {
    // board
    // volume
    cube(size=[100.0, 100.0, 2.0], center=true);
    // components
    // volume
    translate(v=[0.0, 0.0, 11.0]) 
    cube(size=[100.0, 100.0, 20.0], center=true);
    // bolts
    union() {
      translate(v=[-46.0, -46.0, 0]) 
      rotate(a=[0, 180, 0]) 
      union() {
        // thread
        cylinder(h=20, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -11.38]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -62.76]) 
        cylinder(h=100, d=6.8, $fn=53, center=true);
        translate(v=[0, 0, 7.7]) 
        // nut
        cylinder(h=2.8, d=6.8, $fn=6, center=true);
      }
      translate(v=[-46.0, 46.0, 0]) 
      rotate(a=[0, 180, 0]) 
      union() {
        // thread
        cylinder(h=20, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -11.38]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -62.76]) 
        cylinder(h=100, d=6.8, $fn=53, center=true);
        translate(v=[0, 0, 7.7]) 
        // nut
        cylinder(h=2.8, d=6.8, $fn=6, center=true);
      }
      translate(v=[46.0, 46.0, 0]) 
      rotate(a=[0, 180, 0]) 
      union() {
        // thread
        cylinder(h=20, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -11.38]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -62.76]) 
        cylinder(h=100, d=6.8, $fn=53, center=true);
        translate(v=[0, 0, 7.7]) 
        // nut
        cylinder(h=2.8, d=6.8, $fn=6, center=true);
      }
      translate(v=[46.0, -46.0, 0]) 
      rotate(a=[0, 180, 0]) 
      union() {
        // thread
        cylinder(h=20, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -11.38]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -62.76]) 
        cylinder(h=100, d=6.8, $fn=53, center=true);
        translate(v=[0, 0, 7.7]) 
        // nut
        cylinder(h=2.8, d=6.8, $fn=6, center=true);
      }
    }
  }
  // z_bolt
  translate(v=[0.0, 11.91, 152.2]) 
  rotate(a=[270, 0, 180]) 
  union() {
    // thread
    cylinder(h=10, d=6.374, $fn=50, center=true);
    // head
    translate(v=[0, 0, -8.18]) 
    cylinder(h=6.4, d=11.9, $fn=93, center=true);
    // head_clearance
    translate(v=[0, 0, -61.36]) 
    cylinder(h=100, d=11.9, $fn=93, center=true);
  }
  // y_bolt
  translate(v=[0.0, 137.1, 27.01]) 
  rotate(a=[0, 180, 0]) 
  union() {
    // thread
    cylinder(h=10, d=6.374, $fn=50, center=true);
    // head
    translate(v=[0, 0, -8.18]) 
    cylinder(h=6.4, d=11.9, $fn=93, center=true);
    // head_clearance
    translate(v=[0, 0, -61.36]) 
    cylinder(h=100, d=11.9, $fn=93, center=true);
  }
}