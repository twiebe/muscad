difference() {
  union() {
    // tunnel
    difference() {
      // volume
      translate(v=[-0.189, 26.92, -19.565]) 
      cube(size=[19.2, 15.3, 39.13], center=true);
      // volume
      translate(v=[-0.189, 26.92, -19.565]) 
      cube(size=[17.4, 13.5, 39.17], center=true);
    }
    // blower
    difference() {
      hull() 
      {
        // volume
        translate(v=[0.0, 39.67, -53.31]) 
        cube(size=[30.0, 5.0, 0.02], center=true);
        // volume
        translate(v=[-0.189, 26.92, -39.14]) 
        cube(size=[19.2, 15.3, 0.02], center=true);
      }
      hull() 
      {
        // volume
        translate(v=[0.0, 40.67, -54.31]) 
        cube(size=[28.0, 3.0, 0.02], center=true);
        // volume
        translate(v=[-0.189, 26.92, -39.12]) 
        cube(size=[17.2, 13.3, 0.02], center=true);
      }
    }
    // bolt_holder_left
    difference() {
      // volume
      translate(v=[-13.789, 20.77, -17.13]) 
      cube(size=[8.04, 3.0, 15.0], center=true);
      // top_left_chamfer
      translate(v=[-9.829, 20.77, -17.61]) 
      rotate(a=[0, 270, 0]) 
      rotate(a=[90, 0, 0]) 
      difference() {
        // box
        translate(v=[4.0, 4.0, 0.0]) 
        cube(size=[8, 8, 3.04], center=true);
        // chamfer
        rotate(a=[0, 0, 45]) 
        cube(size=[11.3137, 11.3137, 5.04], center=true);
      }
      // bottom_left_chamfer
      translate(v=[-15.809, 20.77, -22.63]) 
      rotate(a=[0, 180, 0]) 
      rotate(a=[90, 0, 0]) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        cube(size=[2.04, 2.04, 3.04], center=true);
        // fillet
        cylinder(h=5.04, d=4, $fn=31, center=true);
      }
    }
    // bolt_holder_right
    translate(v=[-0.189, 0, 0]) 
    mirror(v=[1, 0, 0]) 
    translate(v=[0.189, 0, 0]) 
    // bolt_holder_left
    difference() {
      // volume
      translate(v=[-13.789, 20.77, -17.13]) 
      cube(size=[8.04, 3.0, 15.0], center=true);
      // top_left_chamfer
      translate(v=[-9.829, 20.77, -17.61]) 
      rotate(a=[0, 270, 0]) 
      rotate(a=[90, 0, 0]) 
      difference() {
        // box
        translate(v=[4.0, 4.0, 0.0]) 
        cube(size=[8, 8, 3.04], center=true);
        // chamfer
        rotate(a=[0, 0, 45]) 
        cube(size=[11.3137, 11.3137, 5.04], center=true);
      }
      // bottom_left_chamfer
      translate(v=[-15.809, 20.77, -22.63]) 
      rotate(a=[0, 180, 0]) 
      rotate(a=[90, 0, 0]) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        cube(size=[2.04, 2.04, 3.04], center=true);
        // fillet
        cylinder(h=5.04, d=4, $fn=31, center=true);
      }
    }
  }
  // bolt_left
  translate(v=[-13.789, 17.78, -19.13]) 
  rotate(a=[90, 0, 0]) 
  union() {
    // thread
    cylinder(h=10, d=3.38, $fn=26, center=true);
    // head
    translate(v=[0, 0, -6.38]) 
    cylinder(h=2.8, d=6.8, $fn=53, center=true);
    // head_clearance
    translate(v=[0, 0, -57.76]) 
    cylinder(h=100, d=6.8, $fn=53, center=true);
  }
  // bolt_right
  translate(v=[-0.189, 0, 0]) 
  mirror(v=[1, 0, 0]) 
  translate(v=[-13.6, 17.78, -19.13]) 
  rotate(a=[90, 0, 0]) 
  union() {
    // thread
    cylinder(h=10, d=3.38, $fn=26, center=true);
    // head
    translate(v=[0, 0, -6.38]) 
    cylinder(h=2.8, d=6.8, $fn=53, center=true);
    // head_clearance
    translate(v=[0, 0, -57.76]) 
    cylinder(h=100, d=6.8, $fn=53, center=true);
  }
}