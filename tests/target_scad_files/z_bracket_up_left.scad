difference() {
  union() {
    // body
    difference() {
      // volume
      translate(v=[7.0, 10.6, 32.5]) 
      cube(size=[20.4, 33.2, 57.0], center=true);
      // front_right_chamfer
      translate(v=[7.2, 17.2, 3.98]) 
      linear_extrude(height=57.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[5.0, 5.0, 0.0]) 
        square(size=[10.04, 10.04], center=true);
        // fillet
        circle(d=20, $fn=157);
      }
      // front_left_chamfer
      translate(v=[6.8, 17.2, 3.98]) 
      rotate(a=[0, 0, 90]) 
      linear_extrude(height=57.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[5.0, 5.0, 0.0]) 
        square(size=[10.04, 10.04], center=true);
        // fillet
        circle(d=20, $fn=157);
      }
      // back_right_chamfer
      translate(v=[13.2, -2.0, 3.98]) 
      rotate(a=[0, 0, 270]) 
      linear_extrude(height=57.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        square(size=[4.04, 4.04], center=true);
        // fillet
        circle(d=8, $fn=62);
      }
      // back_left_chamfer
      translate(v=[0.8, -2.0, 3.98]) 
      rotate(a=[0, 0, 180]) 
      linear_extrude(height=57.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        square(size=[4.04, 4.04], center=true);
        // fillet
        circle(d=8, $fn=62);
      }
      // bottom_right_chamfer
      translate(v=[13.2, 27.22, 8.0]) 
      rotate(a=[90, 90, 0]) 
      linear_extrude(height=33.24, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        square(size=[4.04, 4.04], center=true);
        // fillet
        circle(d=8, $fn=62);
      }
      // bottom_left_chamfer
      translate(v=[0.8, 27.22, 8.0]) 
      rotate(a=[90, 180, 0]) 
      linear_extrude(height=33.24, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        square(size=[4.04, 4.04], center=true);
        // fillet
        circle(d=8, $fn=62);
      }
    }
    // rod_holder
    translate(v=[0, 0, 51.0]) 
    linear_extrude(height=10, center=false, convexity=10, twist=0.0, scale=1.0) 
    hull() 
    {
      translate(v=[0.8, -2.0, 0]) 
      circle(d=8, $fn=62);
      translate(v=[12.8, -28.2, 0]) 
      circle(d=6, $fn=47);
      translate(v=[29.2, -28.2, 0]) 
      circle(d=6, $fn=47);
      translate(v=[30.2, -3.8, 0]) 
      circle(d=4, $fn=31);
    }
    // rounding
    translate(v=[23.2, 4.52, 51.0]) 
    rotate(a=[0, 0, 180]) 
    linear_extrude(height=10.0, center=false, convexity=10, twist=0.0, scale=1.0) 
    difference() {
      // box
      translate(v=[3.0, 3.0, 0.0]) 
      square(size=[6.04, 6.04], center=true);
      // fillet
      circle(d=12, $fn=94);
    }
  }
  // extrusion
  #translate(v=[-25.0, 15.1, 15.1]) 
  rotate(a=[0, 90, 0]) 
  // profile
  linear_extrude(height=50, center=false, convexity=10, twist=0.0, scale=1.0) 
  hull() 
  {
    translate(v=[-13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[-13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
  }
  // rod
  #translate(v=[21.0, -22.0, 36.02]) 
  // rod
  cylinder(h=50, d=12.4, $fn=97, center=true);
  // rod_holder_bolt
  translate(v=[22.22, -10.8, 56.0]) 
  rotate(a=[0, 90, 0]) 
  union() {
    // thread
    cylinder(h=20, d=3.38, $fn=26, center=true);
    // head
    translate(v=[0, 0, -11.38]) 
    cylinder(h=2.8, d=6.8, $fn=53, center=true);
    // head_clearance
    translate(v=[0, 0, -62.76]) 
    cylinder(h=100, d=6.8, $fn=53, center=true);
    rotate(a=[0, 0, 90]) 
    translate(v=[0, 0, 8.6]) 
    // nut
    cylinder(h=2.8, d=6.8, $fn=6, center=true);
  }
  // rod_holder_clearance
  // volume
  translate(v=[21.0, -6.8, 56.0]) 
  cube(size=[2.0, 20.0, 11.0], center=true);
  // top_bolt
  translate(v=[7.0, 15.1, 29.01]) 
  rotate(a=[180, 0, 0]) 
  union() {
    // thread
    cylinder(h=12, d=6.374, $fn=50, center=true);
    // head
    translate(v=[0, 0, -9.18]) 
    cylinder(h=6.4, d=11.9, $fn=93, center=true);
    // head_clearance
    translate(v=[0, 0, -62.36]) 
    cylinder(h=100, d=11.9, $fn=93, center=true);
  }
  union() {
    hull() 
    {
      translate(v=[7.0, 1.19, 15.1]) 
      rotate(a=[270, 0, 0]) 
      // thread
      cylinder(h=12, d=6.374, $fn=50, center=true);
      translate(v=[7.0, 1.19, -4.9]) 
      rotate(a=[270, 0, 0]) 
      // thread
      cylinder(h=12, d=6.374, $fn=50, center=true);
    }
    hull() 
    {
      translate(v=[7.0, 1.19, 15.1]) 
      rotate(a=[270, 0, 0]) 
      translate(v=[0, 0, -9.18]) 
      cylinder(h=6.4, d=11.9, $fn=93, center=true);
      translate(v=[7.0, 1.19, -4.9]) 
      rotate(a=[270, 0, 0]) 
      translate(v=[0, 0, -9.18]) 
      cylinder(h=6.4, d=11.9, $fn=93, center=true);
    }
    hull() 
    {
      translate(v=[7.0, 1.19, 15.1]) 
      rotate(a=[270, 0, 0]) 
      translate(v=[0, 0, -62.36]) 
      cylinder(h=100, d=11.9, $fn=93, center=true);
      translate(v=[7.0, 1.19, -4.9]) 
      rotate(a=[270, 0, 0]) 
      translate(v=[0, 0, -62.36]) 
      cylinder(h=100, d=11.9, $fn=93, center=true);
    }
  }
}