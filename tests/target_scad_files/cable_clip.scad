difference() {
  // body
  difference() {
    // volume
    translate(v=[0.0, 1.0, 0.0]) 
    cube(size=[12.0, 5.0, 6.0], center=true);
    // back_right_chamfer
    translate(v=[5.0, -0.5, -3.02]) 
    rotate(a=[0, 0, 270]) 
    linear_extrude(height=6.04, center=false, convexity=10, twist=0.0, scale=1.0) 
    difference() {
      // box
      translate(v=[0.5, 0.5, 0.0]) 
      square(size=[1.04, 1.04], center=true);
      // fillet
      circle(d=2, $fn=15);
    }
    // back_left_chamfer
    translate(v=[-5.0, -0.5, -3.02]) 
    rotate(a=[0, 0, 180]) 
    linear_extrude(height=6.04, center=false, convexity=10, twist=0.0, scale=1.0) 
    difference() {
      // box
      translate(v=[0.5, 0.5, 0.0]) 
      square(size=[1.04, 1.04], center=true);
      // fillet
      circle(d=2, $fn=15);
    }
    // front_right_chamfer
    translate(v=[2.0, -0.5, -3.02]) 
    linear_extrude(height=6.04, center=false, convexity=10, twist=0.0, scale=1.0) 
    difference() {
      // box
      translate(v=[2.0, 2.0, 0.0]) 
      square(size=[4.04, 4.04], center=true);
      // fillet
      circle(d=8, $fn=62);
    }
    // front_left_chamfer
    translate(v=[-2.0, -0.5, -3.02]) 
    rotate(a=[0, 0, 90]) 
    linear_extrude(height=6.04, center=false, convexity=10, twist=0.0, scale=1.0) 
    difference() {
      // box
      translate(v=[2.0, 2.0, 0.0]) 
      square(size=[4.04, 4.04], center=true);
      // fillet
      circle(d=8, $fn=62);
    }
  }
  // center_clearance
  difference() {
    // volume
    translate(v=[0.0, 1.76, 0.0]) 
    cube(size=[6.0, 3.52, 6.04], center=true);
    // back_right_chamfer
    translate(v=[2.0, 1.0, -3.04]) 
    rotate(a=[0, 0, 270]) 
    linear_extrude(height=6.08, center=false, convexity=10, twist=0.0, scale=1.0) 
    difference() {
      // box
      translate(v=[0.5, 0.5, 0.0]) 
      square(size=[1.04, 1.04], center=true);
      // fillet
      circle(d=2, $fn=15);
    }
    // back_left_chamfer
    translate(v=[-2.0, 1.0, -3.04]) 
    rotate(a=[0, 0, 180]) 
    linear_extrude(height=6.08, center=false, convexity=10, twist=0.0, scale=1.0) 
    difference() {
      // box
      translate(v=[0.5, 0.5, 0.0]) 
      square(size=[1.04, 1.04], center=true);
      // fillet
      circle(d=2, $fn=15);
    }
  }
  // side_clearance_right
  // volume
  translate(v=[6.5, 1.0, 0.0]) 
  cube(size=[5, 2, 6.04], center=true);
  // side_clearance_left
  mirror(v=[1, 0, 0]) 
  // side_clearance_right
  // volume
  translate(v=[6.5, 1.0, 0.0]) 
  cube(size=[5, 2, 6.04], center=true);
}