difference() {
  union() {
    // body
    difference() {
      // volume
      translate(v=[23.1, -7.1, -5.0]) 
      cube(size=[42.2, 42.2, 69.4], center=true);
      // front_right_chamfer
      translate(v=[42.2, 12.0, -39.72]) 
      linear_extrude(height=69.44, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
      // back_right_chamfer
      translate(v=[42.2, -26.2, -39.72]) 
      rotate(a=[0, 0, 270]) 
      linear_extrude(height=69.44, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
      // back_bottom_chamfer
      translate(v=[1.98, -26.2, -37.7]) 
      rotate(a=[270, 0, 0]) 
      rotate(a=[0, 90, 0]) 
      linear_extrude(height=42.24, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
      // back_top_chamfer
      translate(v=[1.98, -26.2, 27.7]) 
      rotate(a=[180, 0, 0]) 
      rotate(a=[0, 90, 0]) 
      linear_extrude(height=42.24, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
      // bottom_left_chamfer
      translate(v=[4.0, 14.02, -37.7]) 
      rotate(a=[90, 180, 0]) 
      linear_extrude(height=42.24, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
      // top_left_chamfer
      translate(v=[4.0, 14.02, 27.7]) 
      rotate(a=[90, 270, 0]) 
      linear_extrude(height=42.24, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
    }
    // pulleys_holder
    difference() {
      // volume
      translate(v=[50.375, -7.1, -5.0]) 
      cube(size=[40.35, 42.2, 31.7], center=true);
      // top_right_chamfer
      translate(v=[68.55, 14.02, 8.85]) 
      rotate(a=[90, 0, 0]) 
      linear_extrude(height=42.24, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
      // bottom_right_chamfer
      translate(v=[68.55, 14.02, -18.85]) 
      rotate(a=[90, 90, 0]) 
      linear_extrude(height=42.24, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
    }
  }
  // outer_y_pulley
  translate(v=[51.35, -17.1, 1.35]) 
  union() {
    // body
    cylinder(h=10.7, d=18.4, $fn=144, center=true);
    // clearance
    rotate(a=[0, 0, 180]) 
    translate(v=[0.0, 10.0, 0.0]) 
    cube(size=[18.4, 20, 10.7], center=true);
    // belt_clearance
    translate(v=[-4.6, 20.0, 0.0]) 
    cube(size=[9.2, 40, 10.7], center=true);
    // belt_clearance
    rotate(a=[0, 0, 270]) 
    translate(v=[4.6, 35.0, 0.0]) 
    cube(size=[9.2, 70, 10.7], center=true);
    // bolt
    translate(v=[0, 0, -1.93]) 
    rotate(a=[180, 0, 0]) 
    union() {
      // thread
      cylinder(h=20, d=3.38, $fn=26, center=true);
      // head
      translate(v=[0, 0, -11.38]) 
      cylinder(h=2.8, d=6.8, $fn=53, center=true);
      // head_clearance
      translate(v=[0, 0, -62.76]) 
      cylinder(h=100, d=6.8, $fn=53, center=true);
      translate(v=[0, 0, 8.1]) 
      // nut
      cylinder(h=2.8, d=6.8, $fn=6, center=true);
      // nut_clearance
      translate(v=[15.0, 0.0, 8.1]) 
      cube(size=[30, 6.089, 2.8], center=true);
    }
  }
  // x_pulley
  translate(v=[61.35, -17.1, -12.35]) 
  union() {
    // body
    cylinder(h=10.7, d=18.4, $fn=144, center=true);
    // clearance
    rotate(a=[0, 0, 270]) 
    translate(v=[0.0, 5.0, 0.0]) 
    cube(size=[18.4, 10, 10.7], center=true);
    // belt_clearance
    rotate(a=[0, 0, 270]) 
    translate(v=[4.6, 5.0, 0.0]) 
    cube(size=[9.2, 10, 10.7], center=true);
    // belt_clearance
    translate(v=[-4.6, 20.0, 0.0]) 
    cube(size=[9.2, 40, 10.7], center=true);
    // bolt
    translate(v=[0, 0, 1.39]) 
    union() {
      // thread
      cylinder(h=20, d=3.38, $fn=26, center=true);
      // head
      translate(v=[0, 0, -11.38]) 
      cylinder(h=2.8, d=6.8, $fn=53, center=true);
      // head_clearance
      translate(v=[0, 0, -62.76]) 
      cylinder(h=100, d=6.8, $fn=53, center=true);
      translate(v=[0, 0, 8.4]) 
      // nut
      cylinder(h=2.8, d=6.8, $fn=6, center=true);
    }
  }
  // z_extrusion
  #translate(v=[15.1, -15.1, -60.0]) 
  // profile
  linear_extrude(height=120, center=false, convexity=10, twist=0.0, scale=1.0) 
  hull() 
  {
    translate(v=[-13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[-13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
  }
  // y_extrusion
  #translate(v=[15.1, 0.0, 44.9]) 
  rotate(a=[270, 0, 0]) 
  // profile
  linear_extrude(height=100, center=false, convexity=10, twist=0.0, scale=1.0) 
  hull() 
  {
    translate(v=[-13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[-13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
  }
  // x_extrusion
  #translate(v=[30.2, -15.1, 44.9]) 
  rotate(a=[0, 90, 0]) 
  // profile
  linear_extrude(height=100, center=false, convexity=10, twist=0.0, scale=1.0) 
  hull() 
  {
    translate(v=[-13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[-13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
  }
  // y_rod
  #translate(v=[15.1, 14.98, -5.0]) 
  rotate(a=[90, 0, 0]) 
  // rod
  cylinder(h=30, d=12.4, $fn=97, center=true);
  // front_top_bolt
  #translate(v=[15.1, 0.0, 16.8]) 
  rotate(a=[270, 0, 180]) 
  union() {
    // thread
    cylinder(h=12, d=6.374, $fn=50, center=true);
    // head
    translate(v=[0, 0, -9.18]) 
    cylinder(h=6.4, d=11.9, $fn=93, center=true);
    // head_clearance
    translate(v=[0, 0, -62.36]) 
    cylinder(h=100, d=11.9, $fn=93, center=true);
  }
  // front_bottom_bolt
  translate(v=[0, 0, -5.0]) 
  mirror(v=[0, 0, 1]) 
  translate(v=[15.1, 0.0, 21.8]) 
  rotate(a=[270, 0, 180]) 
  union() {
    // thread
    cylinder(h=12, d=6.374, $fn=50, center=true);
    // head
    translate(v=[0, 0, -9.18]) 
    cylinder(h=6.4, d=11.9, $fn=93, center=true);
    // head_clearance
    translate(v=[0, 0, -62.36]) 
    cylinder(h=100, d=11.9, $fn=93, center=true);
  }
  // right_top_bolt
  translate(v=[28.2, -15.1, 16.8]) 
  rotate(a=[270, 0, 90]) 
  union() {
    // thread
    cylinder(h=12, d=6.374, $fn=50, center=true);
    // head
    translate(v=[0, 0, -9.18]) 
    cylinder(h=6.4, d=11.9, $fn=93, center=true);
    // head_clearance
    translate(v=[0, 0, -62.36]) 
    cylinder(h=100, d=11.9, $fn=93, center=true);
  }
  // right_bottom_bolt
  translate(v=[0, 0, -5.0]) 
  mirror(v=[0, 0, 1]) 
  translate(v=[28.2, -15.1, 21.8]) 
  rotate(a=[270, 0, 90]) 
  union() {
    // thread
    cylinder(h=12, d=6.374, $fn=50, center=true);
    // head
    translate(v=[0, 0, -9.18]) 
    cylinder(h=6.4, d=11.9, $fn=93, center=true);
    // head_clearance
    translate(v=[0, 0, -62.36]) 
    cylinder(h=100, d=11.9, $fn=93, center=true);
  }
  // inner_y_pulley
  translate(v=[61.35, 0.0, 1.35]) 
  union() {
    // body
    cylinder(h=10.7, d=18.4, $fn=144, center=true);
    // clearance
    rotate(a=[0, 0, 270]) 
    translate(v=[0.0, 5.0, 0.0]) 
    cube(size=[18.4, 10, 10.7], center=true);
    // belt_clearance
    rotate(a=[0, 0, 270]) 
    translate(v=[4.6, 5.0, 0.0]) 
    cube(size=[9.2, 10, 10.7], center=true);
    // belt_clearance
    translate(v=[-4.6, 20.0, 0.0]) 
    cube(size=[9.2, 40, 10.7], center=true);
    // bolt
    translate(v=[0, 0, -1.39]) 
    rotate(a=[180, 0, 0]) 
    union() {
      // thread
      cylinder(h=20, d=3.38, $fn=26, center=true);
      // head
      translate(v=[0, 0, -11.38]) 
      cylinder(h=2.8, d=6.8, $fn=53, center=true);
      // head_clearance
      translate(v=[0, 0, -62.76]) 
      cylinder(h=100, d=6.8, $fn=53, center=true);
      translate(v=[0, 0, 8.68]) 
      // nut
      cylinder(h=2.8, d=6.8, $fn=6, center=true);
    }
  }
  // clamp_clearance
  union() {
    // volume
    translate(v=[15.1, -7.1, 2.425]) 
    cube(size=[1.0, 42.24, 14.85], center=true);
    // volume
    translate(v=[8.54, -7.1, 9.35]) 
    cube(size=[13.12, 42.24, 1.0], center=true);
  }
  // tightening_bolt
  translate(v=[15.78, 7.0, 4.9]) 
  rotate(a=[270, 0, 270]) 
  union() {
    // thread
    cylinder(h=16, d=3.38, $fn=26, center=true);
    // head
    translate(v=[0, 0, -9.38]) 
    cylinder(h=2.8, d=6.8, $fn=53, center=true);
    // head_clearance
    translate(v=[0, 0, -60.76]) 
    cylinder(h=100, d=6.8, $fn=53, center=true);
    rotate(a=[0, 0, 180]) 
    translate(v=[0, 0, 6.6]) 
    // nut
    cylinder(h=2.8, d=6.8, $fn=6, center=true);
    // nut_clearance
    rotate(a=[0, 0, 180]) 
    translate(v=[10.0, 0.0, 6.6]) 
    cube(size=[20, 6.089, 2.8], center=true);
  }
  // endstop
  translate(v=[50.72, 10.77, -23.87]) 
  union() {
    // enstop
    cube(size=[13, 6.5, 6], center=true);
    // cables
    translate(v=[0.0, -8.25, 0.0]) 
    cube(size=[13, 10, 6], center=true);
    // left_bolt
    translate(v=[-3.25, -1.75, 0.0]) 
    cylinder(h=12, d=1, $fn=7, center=true);
    // right_bolt
    translate(v=[3.25, -1.75, 0.0]) 
    cylinder(h=12, d=1, $fn=7, center=true);
  }
}