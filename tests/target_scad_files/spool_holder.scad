difference() {
  union() {
    // stopper_front
    difference() {
      // volume
      translate(v=[0.0, 105.1, 20.007]) 
      cube(size=[30.0, 4.0, 24.186], center=true);
      // top_right_chamfer
      translate(v=[3.0, 107.12, 20.1]) 
      rotate(a=[90, 0, 0]) 
      linear_extrude(height=4.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[6.0, 6.0, 0.0]) 
        square(size=[12.04, 12.04], center=true);
        // fillet
        circle(d=24, $fn=188);
      }
      // top_left_chamfer
      translate(v=[-3.0, 107.12, 20.1]) 
      rotate(a=[90, 270, 0]) 
      linear_extrude(height=4.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[6.0, 6.0, 0.0]) 
        square(size=[12.04, 12.04], center=true);
        // fillet
        circle(d=24, $fn=188);
      }
      // bottom_right_chamfer
      translate(v=[3.0, 107.12, 19.914]) 
      rotate(a=[90, 90, 0]) 
      linear_extrude(height=4.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[6.0, 6.0, 0.0]) 
        square(size=[12.04, 12.04], center=true);
        // fillet
        circle(d=24, $fn=188);
      }
    }
    // arm
    difference() {
      // volume
      translate(v=[0.0, 59.15, 16.1]) 
      cube(size=[30.0, 88.1, 16.372], center=true);
      // top_right_chamfer
      translate(v=[7.0, 103.22, 16.286]) 
      rotate(a=[90, 0, 0]) 
      linear_extrude(height=88.14, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[4.0, 4.0, 0.0]) 
        square(size=[8.04, 8.04], center=true);
        // fillet
        circle(d=16, $fn=125);
      }
      // top_left_chamfer
      translate(v=[-7.0, 103.22, 16.286]) 
      rotate(a=[90, 270, 0]) 
      linear_extrude(height=88.14, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[4.0, 4.0, 0.0]) 
        square(size=[8.04, 8.04], center=true);
        // fillet
        circle(d=16, $fn=125);
      }
      // bottom_right_chamfer
      translate(v=[3.0, 103.22, 19.914]) 
      rotate(a=[90, 90, 0]) 
      linear_extrude(height=88.14, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[6.0, 6.0, 0.0]) 
        square(size=[12.04, 12.04], center=true);
        // fillet
        circle(d=24, $fn=188);
      }
    }
    // fix
    difference() {
      // volume
      translate(v=[1.0, 4.0, 4.0]) 
      cube(size=[32.0, 34.2, 34.2], center=true);
      // top_right_chamfer
      translate(v=[13.0, 21.12, 17.1]) 
      rotate(a=[90, 0, 0]) 
      linear_extrude(height=34.24, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        square(size=[4.04, 4.04], center=true);
        // fillet
        circle(d=8, $fn=62);
      }
      // top_left_chamfer
      translate(v=[-11.0, 21.12, 17.1]) 
      rotate(a=[90, 270, 0]) 
      linear_extrude(height=34.24, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        square(size=[4.04, 4.04], center=true);
        // fillet
        circle(d=8, $fn=62);
      }
      // bottom_right_chamfer
      translate(v=[9.0, 21.12, -5.1]) 
      rotate(a=[90, 90, 0]) 
      linear_extrude(height=34.24, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[4.0, 4.0, 0.0]) 
        square(size=[8.04, 8.04], center=true);
        // fillet
        circle(d=16, $fn=125);
      }
      // back_right_chamfer
      translate(v=[1.0, 2.9, -13.12]) 
      rotate(a=[0, 0, 270]) 
      linear_extrude(height=34.24, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[8.0, 8.0, 0.0]) 
        square(size=[16.04, 16.04], center=true);
        // fillet
        circle(d=32, $fn=251);
      }
      // back_left_chamfer
      translate(v=[1.0, 2.9, -13.12]) 
      rotate(a=[0, 0, 180]) 
      linear_extrude(height=34.24, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[8.0, 8.0, 0.0]) 
        square(size=[16.04, 16.04], center=true);
        // fillet
        circle(d=32, $fn=251);
      }
    }
    // reinforcement
    hull() 
    {
      // volume
      translate(v=[-12.0, 21.58, -13.09]) 
      cube(size=[6.0, 1.0, 0.02], center=true);
      // volume
      translate(v=[-12.0, 21.58, 7.924]) 
      cube(size=[6.0, 1.0, 0.02], center=true);
      // volume
      translate(v=[-12.0, 106.6, 7.924]) 
      cube(size=[6.0, 1.0, 0.02], center=true);
    }
    // stopper_back
    difference() {
      // volume
      translate(v=[1.0, 18.1, 23.6]) 
      cube(size=[32.0, 6.0, 17.0], center=true);
      // top_right_chamfer
      translate(v=[5.0, 21.12, 20.1]) 
      rotate(a=[90, 0, 0]) 
      linear_extrude(height=6.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[6.0, 6.0, 0.0]) 
        square(size=[12.04, 12.04], center=true);
        // fillet
        circle(d=24, $fn=188);
      }
      // top_left_chamfer
      translate(v=[-3.0, 21.12, 20.1]) 
      rotate(a=[90, 270, 0]) 
      linear_extrude(height=6.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[6.0, 6.0, 0.0]) 
        square(size=[12.04, 12.04], center=true);
        // fillet
        circle(d=24, $fn=188);
      }
    }
  }
  // extrusion
  #translate(v=[-20.0, 0, 0.0]) 
  rotate(a=[0, 90, 0]) 
  // profile
  linear_extrude(height=40, center=false, convexity=10, twist=0.0, scale=1.0) 
  hull() 
  {
    translate(v=[-13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[-13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
  }
  hull() 
  {
    translate(v=[0.0, 59.1, 18.1]) 
    rotate(a=[270, 0, 180]) 
    // thread
    cylinder(h=61.6, d=8.372, $fn=65, center=true);
    translate(v=[0.0, 59.1, 26.1]) 
    rotate(a=[270, 0, 180]) 
    // thread
    cylinder(h=61.6, d=8.372, $fn=65, center=true);
  }
  // bearing_center
  #translate(v=[0.0, 59.1, 18.1]) 
  rotate(a=[270, 0, 180]) 
  // outer
  cylinder(h=9, d=24, $fn=188, center=true);
  // bearing_front
  #translate(v=[0.0, 81.1, 18.1]) 
  rotate(a=[270, 0, 180]) 
  // outer
  cylinder(h=9, d=24, $fn=188, center=true);
  // bearing_back
  #translate(v=[0.0, 37.1, 18.1]) 
  rotate(a=[270, 0, 180]) 
  // outer
  cylinder(h=9, d=24, $fn=188, center=true);
  // bolt_front
  translate(v=[5.0, 11.91, 0.0]) 
  union() {
    hull() 
    {
      rotate(a=[270, 0, 180]) 
      // thread
      cylinder(h=16, d=6.374, $fn=50, center=true);
      translate(v=[20, 0, 0]) 
      rotate(a=[270, 0, 180]) 
      // thread
      cylinder(h=16, d=6.374, $fn=50, center=true);
    }
    hull() 
    {
      rotate(a=[270, 0, 180]) 
      translate(v=[0, 0, -11.18]) 
      cylinder(h=6.4, d=11.9, $fn=93, center=true);
      translate(v=[20, 0, 0]) 
      rotate(a=[270, 0, 180]) 
      translate(v=[0, 0, -11.18]) 
      cylinder(h=6.4, d=11.9, $fn=93, center=true);
    }
    hull() 
    {
      rotate(a=[270, 0, 180]) 
      translate(v=[0, 0, -64.36]) 
      cylinder(h=100, d=11.9, $fn=93, center=true);
      translate(v=[20, 0, 0]) 
      rotate(a=[270, 0, 180]) 
      translate(v=[0, 0, -64.36]) 
      cylinder(h=100, d=11.9, $fn=93, center=true);
    }
  }
  // bolt_top
  translate(v=[1.0, 0.0, 11.91]) 
  rotate(a=[0, 180, 0]) 
  union() {
    // thread
    cylinder(h=12, d=6.374, $fn=50, center=true);
    // head
    translate(v=[0, 0, -9.18]) 
    cylinder(h=6.4, d=11.9, $fn=93, center=true);
    // head_clearance
    translate(v=[0, 0, -62.36]) 
    cylinder(h=100, d=11.9, $fn=93, center=true);
  }
}