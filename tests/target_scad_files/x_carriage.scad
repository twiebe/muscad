difference() {
  union() {
    // body
    difference() {
      // volume
      translate(v=[0.0, 15.17, 0.0]) 
      cube(size=[61.56, 8.0, 78.26], center=true);
      // top_right_chamfer
      translate(v=[28.78, 19.19, 37.13]) 
      rotate(a=[90, 0, 0]) 
      linear_extrude(height=8.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
      // bottom_right_chamfer
      translate(v=[28.78, 19.19, -37.13]) 
      rotate(a=[90, 90, 0]) 
      linear_extrude(height=8.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
      // bottom_left_chamfer
      translate(v=[-28.78, 19.19, -37.13]) 
      rotate(a=[90, 180, 0]) 
      linear_extrude(height=8.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
      // top_left_chamfer
      translate(v=[-28.78, 19.19, 37.13]) 
      rotate(a=[90, 270, 0]) 
      linear_extrude(height=8.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
    }
    // extruder_holder
    difference() {
      // volume
      translate(v=[0.0, 32.67, 0.0]) 
      cube(size=[40.0, 27.0, 11.0], center=true);
      // top_right_chamfer
      translate(v=[18.0, 46.19, 3.5]) 
      rotate(a=[90, 0, 0]) 
      linear_extrude(height=27.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
      // bottom_right_chamfer
      translate(v=[18.0, 46.19, -3.5]) 
      rotate(a=[90, 90, 0]) 
      linear_extrude(height=27.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
      // bottom_left_chamfer
      translate(v=[-18.0, 46.19, -3.5]) 
      rotate(a=[90, 180, 0]) 
      linear_extrude(height=27.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
      // top_left_chamfer
      translate(v=[-18.0, 46.19, 3.5]) 
      rotate(a=[90, 270, 0]) 
      linear_extrude(height=27.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
    }
    // left_blower_holder
    difference() {
      // volume
      translate(v=[-35.3595, 17.17, 11.4]) 
      cube(size=[13.159, 4.0, 10.5], center=true);
      // bottom_left_chamfer
      translate(v=[-35.939, 19.19, 12.15]) 
      rotate(a=[90, 180, 0]) 
      linear_extrude(height=4.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[3.0, 3.0, 0.0]) 
        square(size=[6.04, 6.04], center=true);
        // fillet
        circle(d=12, $fn=94);
      }
      // top_left_chamfer
      translate(v=[-35.939, 19.19, 10.65]) 
      rotate(a=[90, 270, 0]) 
      linear_extrude(height=4.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[3.0, 3.0, 0.0]) 
        square(size=[6.04, 6.04], center=true);
        // fillet
        circle(d=12, $fn=94);
      }
    }
    // cable_holder
    difference() {
      // volume
      translate(v=[10.811, 15.16, 64.11]) 
      cube(size=[22.5, 8.02, 50.0], center=true);
      // top_right_chamfer
      translate(v=[16.061, 19.19, 83.11]) 
      rotate(a=[90, 0, 0]) 
      linear_extrude(height=8.06, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[3.0, 3.0, 0.0]) 
        square(size=[6.04, 6.04], center=true);
        // fillet
        circle(d=12, $fn=94);
      }
      // top_left_chamfer
      translate(v=[5.561, 19.19, 83.11]) 
      rotate(a=[90, 270, 0]) 
      linear_extrude(height=8.06, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[3.0, 3.0, 0.0]) 
        square(size=[6.04, 6.04], center=true);
        // fillet
        circle(d=12, $fn=94);
      }
    }
  }
  // x_top_rod
  translate(v=[0, 0.0, 22.0]) 
  rotate(a=[270, 0, 90]) 
  // rod
  cylinder(h=100, d=8.4, $fn=65, center=true);
  // x_bottom_rod
  mirror(v=[0, 0, 1]) 
  // x_top_rod
  translate(v=[0, 0.0, 22.0]) 
  rotate(a=[270, 0, 90]) 
  // rod
  cylinder(h=100, d=8.4, $fn=65, center=true);
  // x_bearing_top_right
  translate(v=[15.65, 0.0, 22.0]) 
  rotate(a=[0, 270, 90]) 
  union() {
    // base
    // volume
    cube(size=[34.3, 30.3, 22.3], center=true);
    // bolts
    union() {
      translate(v=[-12.0, -9.0, -8.15]) 
      union() {
        // thread
        cylinder(h=10, d=4.278, $fn=33, center=true);
        // head
        translate(v=[0, 0, -6.73]) 
        cylinder(h=3.5, d=8.4, $fn=65, center=true);
        // head_clearance
        translate(v=[0, 0, -58.46]) 
        cylinder(h=100, d=8.4, $fn=65, center=true);
      }
      translate(v=[-12.0, 9.0, -8.15]) 
      union() {
        // thread
        cylinder(h=10, d=4.278, $fn=33, center=true);
        // head
        translate(v=[0, 0, -6.73]) 
        cylinder(h=3.5, d=8.4, $fn=65, center=true);
        // head_clearance
        translate(v=[0, 0, -58.46]) 
        cylinder(h=100, d=8.4, $fn=65, center=true);
      }
      translate(v=[12.0, -9.0, -8.15]) 
      union() {
        // thread
        cylinder(h=10, d=4.278, $fn=33, center=true);
        // head
        translate(v=[0, 0, -6.73]) 
        cylinder(h=3.5, d=8.4, $fn=65, center=true);
        // head_clearance
        translate(v=[0, 0, -58.46]) 
        cylinder(h=100, d=8.4, $fn=65, center=true);
      }
      translate(v=[12.0, 9.0, -8.15]) 
      union() {
        // thread
        cylinder(h=10, d=4.278, $fn=33, center=true);
        // head
        translate(v=[0, 0, -6.73]) 
        cylinder(h=3.5, d=8.4, $fn=65, center=true);
        // head_clearance
        translate(v=[0, 0, -58.46]) 
        cylinder(h=100, d=8.4, $fn=65, center=true);
      }
    }
    // rod_clearance
    rotate(a=[90, 0, 0]) 
    hull() 
    {
      cylinder(h=234.3, d=10, $fn=78, center=true);
      translate(v=[0, 10, 0]) 
      cylinder(h=234.3, d=10, $fn=78, center=true);
    }
  }
  // x_bearing_bottom
  translate(v=[0.0, 0.0, -22.0]) 
  rotate(a=[0, 270, 90]) 
  union() {
    // base
    // volume
    cube(size=[34.3, 30.3, 22.3], center=true);
    // bolts
    union() {
      translate(v=[-12.0, -9.0, -8.15]) 
      union() {
        // thread
        cylinder(h=10, d=4.278, $fn=33, center=true);
        // head
        translate(v=[0, 0, -6.73]) 
        cylinder(h=3.5, d=8.4, $fn=65, center=true);
        // head_clearance
        translate(v=[0, 0, -58.46]) 
        cylinder(h=100, d=8.4, $fn=65, center=true);
      }
      translate(v=[-12.0, 9.0, -8.15]) 
      union() {
        // thread
        cylinder(h=10, d=4.278, $fn=33, center=true);
        // head
        translate(v=[0, 0, -6.73]) 
        cylinder(h=3.5, d=8.4, $fn=65, center=true);
        // head_clearance
        translate(v=[0, 0, -58.46]) 
        cylinder(h=100, d=8.4, $fn=65, center=true);
      }
      translate(v=[12.0, -9.0, -8.15]) 
      union() {
        // thread
        cylinder(h=10, d=4.278, $fn=33, center=true);
        // head
        translate(v=[0, 0, -6.73]) 
        cylinder(h=3.5, d=8.4, $fn=65, center=true);
        // head_clearance
        translate(v=[0, 0, -58.46]) 
        cylinder(h=100, d=8.4, $fn=65, center=true);
      }
      translate(v=[12.0, 9.0, -8.15]) 
      union() {
        // thread
        cylinder(h=10, d=4.278, $fn=33, center=true);
        // head
        translate(v=[0, 0, -6.73]) 
        cylinder(h=3.5, d=8.4, $fn=65, center=true);
        // head_clearance
        translate(v=[0, 0, -58.46]) 
        cylinder(h=100, d=8.4, $fn=65, center=true);
      }
    }
    // rod_clearance
    rotate(a=[90, 0, 0]) 
    hull() 
    {
      cylinder(h=234.3, d=10, $fn=78, center=true);
      cylinder(h=234.3, d=10, $fn=78, center=true);
    }
  }
  // x_bearing_top_left
  mirror(v=[1, 0, 0]) 
  // x_bearing_top_right
  translate(v=[15.65, 0.0, 22.0]) 
  rotate(a=[0, 270, 90]) 
  union() {
    // base
    // volume
    cube(size=[34.3, 30.3, 22.3], center=true);
    // bolts
    union() {
      translate(v=[-12.0, -9.0, -8.15]) 
      union() {
        // thread
        cylinder(h=10, d=4.278, $fn=33, center=true);
        // head
        translate(v=[0, 0, -6.73]) 
        cylinder(h=3.5, d=8.4, $fn=65, center=true);
        // head_clearance
        translate(v=[0, 0, -58.46]) 
        cylinder(h=100, d=8.4, $fn=65, center=true);
      }
      translate(v=[-12.0, 9.0, -8.15]) 
      union() {
        // thread
        cylinder(h=10, d=4.278, $fn=33, center=true);
        // head
        translate(v=[0, 0, -6.73]) 
        cylinder(h=3.5, d=8.4, $fn=65, center=true);
        // head_clearance
        translate(v=[0, 0, -58.46]) 
        cylinder(h=100, d=8.4, $fn=65, center=true);
      }
      translate(v=[12.0, -9.0, -8.15]) 
      union() {
        // thread
        cylinder(h=10, d=4.278, $fn=33, center=true);
        // head
        translate(v=[0, 0, -6.73]) 
        cylinder(h=3.5, d=8.4, $fn=65, center=true);
        // head_clearance
        translate(v=[0, 0, -58.46]) 
        cylinder(h=100, d=8.4, $fn=65, center=true);
      }
      translate(v=[12.0, 9.0, -8.15]) 
      union() {
        // thread
        cylinder(h=10, d=4.278, $fn=33, center=true);
        // head
        translate(v=[0, 0, -6.73]) 
        cylinder(h=3.5, d=8.4, $fn=65, center=true);
        // head_clearance
        translate(v=[0, 0, -58.46]) 
        cylinder(h=100, d=8.4, $fn=65, center=true);
      }
    }
    // rod_clearance
    rotate(a=[90, 0, 0]) 
    hull() 
    {
      cylinder(h=234.3, d=10, $fn=78, center=true);
      translate(v=[0, 10, 0]) 
      cylinder(h=234.3, d=10, $fn=78, center=true);
    }
  }
  // center_pulleys_bolt
  translate(v=[0, 10.17, 0.0]) 
  rotate(a=[270, 0, 0]) 
  union() {
    // thread
    cylinder(h=16, d=3.38, $fn=26, center=true);
    // head
    translate(v=[0, 0, -9.38]) 
    cylinder(h=2.8, d=6.8, $fn=53, center=true);
    // head_clearance
    translate(v=[0, 0, -60.76]) 
    cylinder(h=100, d=6.8, $fn=53, center=true);
    translate(v=[0, 0, 6.68]) 
    // nut
    cylinder(h=2.8, d=6.8, $fn=6, center=true);
    // inline_nut_clearance
    hull() 
    {
      translate(v=[0, 0, 6.68]) 
      // nut
      cylinder(h=2.8, d=6.8, $fn=6, center=true);
      translate(v=[0, 0, 16.68]) 
      // nut
      cylinder(h=2.8, d=6.8, $fn=6, center=true);
    }
  }
  // extruder
  #translate(v=[0.0, 46.995, -54.31]) 
  // extruder
  union() {
    translate(v=[0, 0, 0.99]) 
    cylinder(h=2, d1=1, d2=6, $fn=7, center=true);
    translate(v=[0, 0, 2.98]) 
    cylinder(h=2, d=8.06, $fn=6, center=true);
    translate(v=[0, 0, 4.97]) 
    cylinder(h=2, d=5, $fn=39, center=true);
    translate(v=[0, 0, 5.96]) 
    // volume
    translate(v=[0.0, 5.5, 5.25]) 
    cube(size=[16, 20.0, 10.5], center=true);
    translate(v=[0, 0, 18.0]) 
    cylinder(h=3.1, d=4, $fn=31, center=true);
    translate(v=[0, 0, 32.04]) 
    cylinder(h=25, d=22.3, $fn=175, center=true);
    translate(v=[0, 0, 48.03]) 
    cylinder(h=7, d=16, $fn=125, center=true);
    translate(v=[0, 0, 54.42]) 
    cylinder(h=5.8, d=12, $fn=94, center=true);
    translate(v=[0, 0, 60.81]) 
    cylinder(h=7, d=16, $fn=125, center=true);
  }
  // blower
  #translate(v=[-17.189, 26.97, 30.7]) 
  rotate(a=[90, 0, 0]) 
  rotate(a=[0, 0, 180]) 
  union() {
    // fan
    cylinder(h=15.4, d=50.4, $fn=395, center=true);
    // bolt_holders
    hull() 
    {
      translate(v=[-23.0, -19.0, 0.0]) 
      cylinder(h=15.4, d=6.378, $fn=50, center=true);
      translate(v=[19.5, 19.3, 0.0]) 
      cylinder(h=15.4, d=6.378, $fn=50, center=true);
    }
    // blower
    // volume
    translate(v=[-17.0, 22.6, 0.0]) 
    cube(size=[19.4, 45.2, 15.4], center=true);
    // back_bolt
    translate(v=[-23.0, -19.0, 1.88]) 
    union() {
      // thread
      cylinder(h=20, d=4.378, $fn=34, center=true);
      // head
      translate(v=[0, 0, -11.78]) 
      cylinder(h=3.6, d=8.5, $fn=66, center=true);
      // head_clearance
      translate(v=[0, 0, -63.56]) 
      cylinder(h=100, d=8.5, $fn=66, center=true);
      translate(v=[0, 0, 8.28]) 
      // nut
      cylinder(h=3.6, d=8.5, $fn=6, center=true);
      // inline_nut_clearance
      hull() 
      {
        translate(v=[0, 0, 8.28]) 
        // nut
        cylinder(h=3.6, d=8.5, $fn=6, center=true);
        translate(v=[0, 0, 28.28]) 
        // nut
        cylinder(h=3.6, d=8.5, $fn=6, center=true);
      }
    }
    // front_bolt
    translate(v=[19.5, 19.3, 1.88]) 
    union() {
      // thread
      cylinder(h=20, d=4.378, $fn=34, center=true);
      // head
      translate(v=[0, 0, -11.78]) 
      cylinder(h=3.6, d=8.5, $fn=66, center=true);
      // head_clearance
      translate(v=[0, 0, -63.56]) 
      cylinder(h=100, d=8.5, $fn=66, center=true);
      translate(v=[0, 0, 8.28]) 
      // nut
      cylinder(h=3.6, d=8.5, $fn=6, center=true);
      // inline_nut_clearance
      hull() 
      {
        translate(v=[0, 0, 8.28]) 
        // nut
        cylinder(h=3.6, d=8.5, $fn=6, center=true);
        translate(v=[0, 0, 28.28]) 
        // nut
        cylinder(h=3.6, d=8.5, $fn=6, center=true);
      }
    }
  }
  // cable_hole_bottom_left
  difference() {
    // volume
    translate(v=[25.8, 15.17, -31.13]) 
    cube(size=[10.0, 10.0, 10.0], center=true);
    // bottom_left_chamfer
    translate(v=[22.8, 20.19, -34.13]) 
    rotate(a=[90, 180, 0]) 
    linear_extrude(height=10.04, center=false, convexity=10, twist=0.0, scale=1.0) 
    difference() {
      // box
      translate(v=[1.0, 1.0, 0.0]) 
      square(size=[2.04, 2.04], center=true);
      // fillet
      circle(d=4, $fn=31);
    }
    // top_left_chamfer
    translate(v=[22.8, 20.19, -28.13]) 
    rotate(a=[90, 270, 0]) 
    linear_extrude(height=10.04, center=false, convexity=10, twist=0.0, scale=1.0) 
    difference() {
      // box
      translate(v=[1.0, 1.0, 0.0]) 
      square(size=[2.04, 2.04], center=true);
      // fillet
      circle(d=4, $fn=31);
    }
    // top_right_chamfer
    translate(v=[28.82, 15.17, -28.11]) 
    rotate(a=[90, 0, 0]) 
    difference() {
      // box
      translate(v=[1.0, 1.0, 0.0]) 
      cube(size=[2, 2, 10.04], center=true);
      // chamfer
      rotate(a=[0, 0, 45]) 
      cube(size=[2.8284, 2.8284, 12.04], center=true);
    }
    // bottom_right_chamfer
    translate(v=[28.82, 15.17, -34.15]) 
    rotate(a=[90, 90, 0]) 
    difference() {
      // box
      translate(v=[1.0, 1.0, 0.0]) 
      cube(size=[2, 2, 10.04], center=true);
      // chamfer
      rotate(a=[0, 0, 45]) 
      cube(size=[2.8284, 2.8284, 12.04], center=true);
    }
  }
  // cable_hole_bottom_right
  mirror(v=[1, 0, 0]) 
  // cable_hole_bottom_left
  difference() {
    // volume
    translate(v=[25.8, 15.17, -31.13]) 
    cube(size=[10.0, 10.0, 10.0], center=true);
    // bottom_left_chamfer
    translate(v=[22.8, 20.19, -34.13]) 
    rotate(a=[90, 180, 0]) 
    linear_extrude(height=10.04, center=false, convexity=10, twist=0.0, scale=1.0) 
    difference() {
      // box
      translate(v=[1.0, 1.0, 0.0]) 
      square(size=[2.04, 2.04], center=true);
      // fillet
      circle(d=4, $fn=31);
    }
    // top_left_chamfer
    translate(v=[22.8, 20.19, -28.13]) 
    rotate(a=[90, 270, 0]) 
    linear_extrude(height=10.04, center=false, convexity=10, twist=0.0, scale=1.0) 
    difference() {
      // box
      translate(v=[1.0, 1.0, 0.0]) 
      square(size=[2.04, 2.04], center=true);
      // fillet
      circle(d=4, $fn=31);
    }
    // top_right_chamfer
    translate(v=[28.82, 15.17, -28.11]) 
    rotate(a=[90, 0, 0]) 
    difference() {
      // box
      translate(v=[1.0, 1.0, 0.0]) 
      cube(size=[2, 2, 10.04], center=true);
      // chamfer
      rotate(a=[0, 0, 45]) 
      cube(size=[2.8284, 2.8284, 12.04], center=true);
    }
    // bottom_right_chamfer
    translate(v=[28.82, 15.17, -34.15]) 
    rotate(a=[90, 90, 0]) 
    difference() {
      // box
      translate(v=[1.0, 1.0, 0.0]) 
      cube(size=[2, 2, 10.04], center=true);
      // chamfer
      rotate(a=[0, 0, 45]) 
      cube(size=[2.8284, 2.8284, 12.04], center=true);
    }
  }
  // fan
  translate(v=[0.0, 69.37, -14.7]) 
  rotate(a=[90, 0, 0]) 
  union() {
    // body
    difference() {
      // volume
      cube(size=[40.4, 40.4, 20.4], center=true);
      // front_right_chamfer
      translate(v=[18.2, 18.2, -10.22]) 
      linear_extrude(height=20.44, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
      // back_right_chamfer
      translate(v=[18.2, -18.2, -10.22]) 
      rotate(a=[0, 0, 270]) 
      linear_extrude(height=20.44, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
      // back_left_chamfer
      translate(v=[-18.2, -18.2, -10.22]) 
      rotate(a=[0, 0, 180]) 
      linear_extrude(height=20.44, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
      // front_left_chamfer
      translate(v=[-18.2, 18.2, -10.22]) 
      rotate(a=[0, 0, 90]) 
      linear_extrude(height=20.44, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[1.0, 1.0, 0.0]) 
        square(size=[2.04, 2.04], center=true);
        // fillet
        circle(d=4, $fn=31);
      }
    }
    // bolts
    translate(v=[0, 0, 5.08]) 
    {
      rotate(a=[0, 0, 225]) 
      translate(v=[22.6274, 0, 0]) 
      union() {
        // thread
        cylinder(h=25, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -13.88]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -65.26]) 
        cylinder(h=100, d=6.8, $fn=53, center=true);
        translate(v=[0, 0, 11.18]) 
        // nut
        cylinder(h=2.8, d=6.8, $fn=6, center=true);
      }
      rotate(a=[0, 0, 315]) 
      translate(v=[22.6274, 0, 0]) 
      union() {
        // thread
        cylinder(h=25, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -13.88]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -65.26]) 
        cylinder(h=100, d=6.8, $fn=53, center=true);
        translate(v=[0, 0, 11.18]) 
        // nut
        cylinder(h=2.8, d=6.8, $fn=6, center=true);
      }
    }
    // bolts
    translate(v=[0, 0, 12.58]) 
    {
      rotate(a=[0, 0, 45]) 
      translate(v=[22.6274, 0, 0]) 
      union() {
        // thread
        cylinder(h=40, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -21.38]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -72.76]) 
        cylinder(h=100, d=6.8, $fn=53, center=true);
      }
      rotate(a=[0, 0, 135]) 
      translate(v=[22.6274, 0, 0]) 
      union() {
        // thread
        cylinder(h=40, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -21.38]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -72.76]) 
        cylinder(h=100, d=6.8, $fn=53, center=true);
      }
    }
  }
  // extruder_clamp_bolt_right
  translate(v=[-16.0, 44.78, 1.3]) 
  rotate(a=[90, 0, 0]) 
  union() {
    // thread
    cylinder(h=12, d=3.38, $fn=26, center=true);
    // head
    translate(v=[0, 0, -7.38]) 
    cylinder(h=2.8, d=6.8, $fn=53, center=true);
    // head_clearance
    translate(v=[0, 0, -58.76]) 
    cylinder(h=100, d=6.8, $fn=53, center=true);
    rotate(a=[0, 0, 90]) 
    translate(v=[0, 0, 4.68]) 
    // nut
    cylinder(h=2.8, d=6.8, $fn=6, center=true);
    // nut_clearance
    rotate(a=[0, 0, 90]) 
    translate(v=[10.0, 0.0, 4.68]) 
    cube(size=[20, 6.089, 2.8], center=true);
  }
  // extruder_clamp_bolt_left
  mirror(v=[1, 0, 0]) 
  // extruder_clamp_bolt_right
  translate(v=[-16.0, 44.78, 1.3]) 
  rotate(a=[90, 0, 0]) 
  union() {
    // thread
    cylinder(h=12, d=3.38, $fn=26, center=true);
    // head
    translate(v=[0, 0, -7.38]) 
    cylinder(h=2.8, d=6.8, $fn=53, center=true);
    // head_clearance
    translate(v=[0, 0, -58.76]) 
    cylinder(h=100, d=6.8, $fn=53, center=true);
    rotate(a=[0, 0, 90]) 
    translate(v=[0, 0, 4.68]) 
    // nut
    cylinder(h=2.8, d=6.8, $fn=6, center=true);
    // nut_clearance
    rotate(a=[0, 0, 90]) 
    translate(v=[10.0, 0.0, 4.68]) 
    cube(size=[20, 6.089, 2.8], center=true);
  }
}