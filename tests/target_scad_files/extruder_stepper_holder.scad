difference() {
  union() {
    // stepper_holder
    difference() {
      // volume
      translate(v=[-37.25, -62.55, -0.05]) 
      cube(size=[44.3, 34.9, 42.3], center=true);
      // bottom_right_chamfer
      translate(v=[-19.1, -45.08, -17.2]) 
      rotate(a=[90, 90, 0]) 
      linear_extrude(height=34.94, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        square(size=[4.04, 4.04], center=true);
        // fillet
        circle(d=8, $fn=62);
      }
      // bottom_left_chamfer
      translate(v=[-55.4, -45.08, -17.2]) 
      rotate(a=[90, 180, 0]) 
      linear_extrude(height=34.94, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        square(size=[4.04, 4.04], center=true);
        // fillet
        circle(d=8, $fn=62);
      }
      // top_left_chamfer
      translate(v=[-55.4, -45.08, 17.1]) 
      rotate(a=[90, 270, 0]) 
      linear_extrude(height=34.94, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        square(size=[4.04, 4.04], center=true);
        // fillet
        circle(d=8, $fn=62);
      }
    }
    // attachment
    difference() {
      // volume
      translate(v=[-1.01, -40.0, 18.1]) 
      cube(size=[28.22, 80.0, 6.0], center=true);
      // front_right_chamfer
      translate(v=[9.1, -4.0, 15.08]) 
      linear_extrude(height=6.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        square(size=[4.04, 4.04], center=true);
        // fillet
        circle(d=8, $fn=62);
      }
      // back_right_chamfer
      translate(v=[9.1, -76.0, 15.08]) 
      rotate(a=[0, 0, 270]) 
      linear_extrude(height=6.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        square(size=[4.04, 4.04], center=true);
        // fillet
        circle(d=8, $fn=62);
      }
      // front_left_chamfer
      translate(v=[-11.12, -4.0, 15.08]) 
      rotate(a=[0, 0, 90]) 
      linear_extrude(height=6.04, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        square(size=[4.04, 4.04], center=true);
        // fillet
        circle(d=8, $fn=62);
      }
    }
    // attachment_fillet
    translate(v=[-17.14, -43.08, 0]) 
    rotate(a=[0, 0, 270]) 
    translate(v=[0, 0, 15.1]) 
    linear_extrude(height=6.0, center=false, convexity=10, twist=0.0, scale=1.0) 
    difference() {
      // box
      translate(v=[1.0, 1.0, 0.0]) 
      square(size=[2.04, 2.04], center=true);
      // fillet
      circle(d=4, $fn=31);
    }
  }
  // y_extrusion
  #rotate(a=[270, 0, 180]) 
  // profile
  linear_extrude(height=100, center=false, convexity=10, twist=0.0, scale=1.0) 
  hull() 
  {
    translate(v=[-13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[-13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
  }
  // stepper
  #translate(v=[-38.25, 0, -0.05]) 
  rotate(a=[270, 0, 180]) 
  union() {
    // body
    difference() {
      // volume
      translate(v=[0.0, 0.0, 22.5]) 
      cube(size=[42.3, 42.3, 45], center=true);
      // front_right_chamfer
      translate(v=[17.17, 17.17, 22.5]) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        cube(size=[4, 4, 45.04], center=true);
        // chamfer
        rotate(a=[0, 0, 45]) 
        cube(size=[5.6569, 5.6569, 47.04], center=true);
      }
      // back_right_chamfer
      translate(v=[17.17, -17.17, 22.5]) 
      rotate(a=[0, 0, 270]) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        cube(size=[4, 4, 45.04], center=true);
        // chamfer
        rotate(a=[0, 0, 45]) 
        cube(size=[5.6569, 5.6569, 47.04], center=true);
      }
      // back_left_chamfer
      translate(v=[-17.17, -17.17, 22.5]) 
      rotate(a=[0, 0, 180]) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        cube(size=[4, 4, 45.04], center=true);
        // chamfer
        rotate(a=[0, 0, 45]) 
        cube(size=[5.6569, 5.6569, 47.04], center=true);
      }
      // front_left_chamfer
      translate(v=[-17.17, 17.17, 22.5]) 
      rotate(a=[0, 0, 90]) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        cube(size=[4, 4, 45.04], center=true);
        // chamfer
        rotate(a=[0, 0, 45]) 
        cube(size=[5.6569, 5.6569, 47.04], center=true);
      }
    }
    // gearbox
    translate(v=[0.0, 0.0, 56.99]) 
    cylinder(h=24.02, d=36.4, $fn=285, center=true);
    // bolts
    translate(v=[0, 0, 69.0]) 
    {
      rotate(a=[0, 0, 45]) 
      translate(v=[14.1421, 0, 0]) 
      rotate(a=[180, 0, 0]) 
      union() {
        // thread
        cylinder(h=10, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -6.38]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -9.76]) 
        cylinder(h=4, d=6.8, $fn=53, center=true);
      }
      rotate(a=[0, 0, 135]) 
      translate(v=[14.1421, 0, 0]) 
      rotate(a=[180, 0, 0]) 
      union() {
        // thread
        cylinder(h=10, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -6.38]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -9.76]) 
        cylinder(h=4, d=6.8, $fn=53, center=true);
      }
      rotate(a=[0, 0, 225]) 
      translate(v=[14.1421, 0, 0]) 
      rotate(a=[180, 0, 0]) 
      union() {
        // thread
        cylinder(h=10, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -6.38]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -9.76]) 
        cylinder(h=4, d=6.8, $fn=53, center=true);
      }
      rotate(a=[0, 0, 315]) 
      translate(v=[14.1421, 0, 0]) 
      rotate(a=[180, 0, 0]) 
      union() {
        // thread
        cylinder(h=10, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -6.38]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -9.76]) 
        cylinder(h=4, d=6.8, $fn=53, center=true);
      }
    }
    // central_bulge
    translate(v=[0.0, 0.0, 70.0]) 
    cylinder(h=4, d=22.4, $fn=175, center=true);
    // shaft
    translate(v=[0.0, 0.0, 82.48]) 
    cylinder(h=27, d=9.4, $fn=73, center=true);
  }
  // bolts
  union() {
    translate(v=[0, 0, -0.05]) 
    mirror(v=[0, 0, 1]) 
    translate(v=[0, 0, 0.05]) 
    {
      translate(v=[-38.25, 0, 0]) 
      mirror(v=[1, 0, 0]) 
      translate(v=[15.52, -78.61, 15.47]) 
      rotate(a=[270, 0, 0]) 
      union() {
        // thread
        cylinder(h=16, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -9.38]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -60.76]) 
        cylinder(h=100, d=6.8, $fn=53, center=true);
        rotate(a=[0, 0, 270]) 
        translate(v=[0, 0, 2.7]) 
        // nut
        cylinder(h=2.8, d=6.8, $fn=6, center=true);
        // nut_clearance
        rotate(a=[0, 0, 270]) 
        translate(v=[10.0, 0.0, 2.7]) 
        cube(size=[20, 6.089, 2.8], center=true);
      }
      translate(v=[-22.73, -78.61, 15.47]) 
      rotate(a=[270, 0, 0]) 
      union() {
        // thread
        cylinder(h=16, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -9.38]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -60.76]) 
        cylinder(h=100, d=6.8, $fn=53, center=true);
        rotate(a=[0, 0, 270]) 
        translate(v=[0, 0, 2.7]) 
        // nut
        cylinder(h=2.8, d=6.8, $fn=6, center=true);
        // nut_clearance
        rotate(a=[0, 0, 270]) 
        translate(v=[10.0, 0.0, 2.7]) 
        cube(size=[20, 6.089, 2.8], center=true);
      }
    }
    union() {
      translate(v=[-38.25, 0, 0]) 
      mirror(v=[1, 0, 0]) 
      translate(v=[15.52, -78.61, 15.47]) 
      rotate(a=[270, 0, 0]) 
      union() {
        // thread
        cylinder(h=16, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -9.38]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -60.76]) 
        cylinder(h=100, d=6.8, $fn=53, center=true);
        rotate(a=[0, 0, 270]) 
        translate(v=[0, 0, 2.7]) 
        // nut
        cylinder(h=2.8, d=6.8, $fn=6, center=true);
        // nut_clearance
        rotate(a=[0, 0, 270]) 
        translate(v=[10.0, 0.0, 2.7]) 
        cube(size=[20, 6.089, 2.8], center=true);
      }
      translate(v=[-22.73, -78.61, 15.47]) 
      rotate(a=[270, 0, 0]) 
      union() {
        // thread
        cylinder(h=16, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -9.38]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -60.76]) 
        cylinder(h=100, d=6.8, $fn=53, center=true);
        rotate(a=[0, 0, 270]) 
        translate(v=[0, 0, 2.7]) 
        // nut
        cylinder(h=2.8, d=6.8, $fn=6, center=true);
        // nut_clearance
        rotate(a=[0, 0, 270]) 
        translate(v=[10.0, 0.0, 2.7]) 
        cube(size=[20, 6.089, 2.8], center=true);
      }
    }
  }
  // bolt_top_back
  translate(v=[0.0, -62.55, 11.91]) 
  rotate(a=[0, 180, 0]) 
  union() {
    // thread
    cylinder(h=12, d=6.374, $fn=50, center=true);
    // head
    translate(v=[0, 0, -9.18]) 
    cylinder(h=6.4, d=11.9, $fn=93, center=true);
    // head_clearance
    translate(v=[0, 0, -62.36]) 
    cylinder(h=100, d=11.9, $fn=93, center=true);
  }
  // bolt_top_front
  translate(v=[0, -40.0, 0]) 
  mirror(v=[0, 1, 0]) 
  translate(v=[0.0, -22.55, 11.91]) 
  rotate(a=[0, 180, 0]) 
  union() {
    // thread
    cylinder(h=12, d=6.374, $fn=50, center=true);
    // head
    translate(v=[0, 0, -9.18]) 
    cylinder(h=6.4, d=11.9, $fn=93, center=true);
    // head_clearance
    translate(v=[0, 0, -62.36]) 
    cylinder(h=100, d=11.9, $fn=93, center=true);
  }
}