difference() {
  union() {
    // body
    difference() {
      // volume
      translate(v=[10.1, 0.0, -9.6]) 
      cube(size=[32.2, 82.2, 33.2], center=true);
      // front_bottom_chamfer
      translate(v=[-6.02, 36.1, -21.2]) 
      rotate(a=[0, 90, 0]) 
      linear_extrude(height=32.24, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[2.5, 2.5, 0.0]) 
        square(size=[5.04, 5.04], center=true);
        // fillet
        circle(d=10, $fn=78);
      }
      // back_bottom_chamfer
      translate(v=[-6.02, -36.1, -21.2]) 
      rotate(a=[270, 0, 0]) 
      rotate(a=[0, 90, 0]) 
      linear_extrude(height=32.24, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[2.5, 2.5, 0.0]) 
        square(size=[5.04, 5.04], center=true);
        // fillet
        circle(d=10, $fn=78);
      }
      // top_left_chamfer
      translate(v=[-1.0, 41.12, 2.0]) 
      rotate(a=[90, 270, 0]) 
      linear_extrude(height=82.24, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[2.5, 2.5, 0.0]) 
        square(size=[5.04, 5.04], center=true);
        // fillet
        circle(d=10, $fn=78);
      }
      // front_right_chamfer
      translate(v=[21.2, 36.1, -26.22]) 
      linear_extrude(height=33.24, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[2.5, 2.5, 0.0]) 
        square(size=[5.04, 5.04], center=true);
        // fillet
        circle(d=10, $fn=78);
      }
      // back_right_chamfer
      translate(v=[21.2, -36.1, -26.22]) 
      rotate(a=[0, 0, 270]) 
      linear_extrude(height=33.24, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[2.5, 2.5, 0.0]) 
        square(size=[5.04, 5.04], center=true);
        // fillet
        circle(d=10, $fn=78);
      }
    }
    // stepper_box
    difference() {
      // volume
      translate(v=[-23.075, 0.0, -9.6]) 
      cube(size=[46.15, 58.3, 33.2], center=true);
      // back_left_chamfer
      translate(v=[-41.15, -24.15, -26.22]) 
      rotate(a=[0, 0, 180]) 
      linear_extrude(height=33.24, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[2.5, 2.5, 0.0]) 
        square(size=[5.04, 5.04], center=true);
        // fillet
        circle(d=10, $fn=78);
      }
      // front_left_chamfer
      translate(v=[-41.15, 24.15, -26.22]) 
      rotate(a=[0, 0, 90]) 
      linear_extrude(height=33.24, center=false, convexity=10, twist=0.0, scale=1.0) 
      difference() {
        // box
        translate(v=[2.5, 2.5, 0.0]) 
        square(size=[5.04, 5.04], center=true);
        // fillet
        circle(d=10, $fn=78);
      }
    }
  }
  // extrusion
  #translate(v=[15.1, 50.0, -15.1]) 
  rotate(a=[90, 0, 0]) 
  // profile
  linear_extrude(height=100, center=false, convexity=10, twist=0.0, scale=1.0) 
  hull() 
  {
    translate(v=[-13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[-13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, -13.1, 0]) 
    circle(d=4, $fn=31);
    translate(v=[13.1, 13.1, 0]) 
    circle(d=4, $fn=31);
  }
  // stepper
  translate(v=[-22.0, 0, -42.0]) 
  union() {
    // body
    difference() {
      // volume
      translate(v=[0.0, 0.0, 21.0]) 
      cube(size=[42.3, 42.3, 42], center=true);
      // front_right_chamfer
      translate(v=[17.17, 17.17, 21.0]) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        cube(size=[4, 4, 42.04], center=true);
        // chamfer
        rotate(a=[0, 0, 45]) 
        cube(size=[5.6569, 5.6569, 44.04], center=true);
      }
      // back_right_chamfer
      translate(v=[17.17, -17.17, 21.0]) 
      rotate(a=[0, 0, 270]) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        cube(size=[4, 4, 42.04], center=true);
        // chamfer
        rotate(a=[0, 0, 45]) 
        cube(size=[5.6569, 5.6569, 44.04], center=true);
      }
      // back_left_chamfer
      translate(v=[-17.17, -17.17, 21.0]) 
      rotate(a=[0, 0, 180]) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        cube(size=[4, 4, 42.04], center=true);
        // chamfer
        rotate(a=[0, 0, 45]) 
        cube(size=[5.6569, 5.6569, 44.04], center=true);
      }
      // front_left_chamfer
      translate(v=[-17.17, 17.17, 21.0]) 
      rotate(a=[0, 0, 90]) 
      difference() {
        // box
        translate(v=[2.0, 2.0, 0.0]) 
        cube(size=[4, 4, 42.04], center=true);
        // chamfer
        rotate(a=[0, 0, 45]) 
        cube(size=[5.6569, 5.6569, 44.04], center=true);
      }
    }
    // bolts
    translate(v=[0, 0, 43.0]) 
    {
      rotate(a=[0, 0, 45]) 
      translate(v=[21.9486, 0, 0]) 
      rotate(a=[180, 0, 0]) 
      union() {
        // thread
        cylinder(h=8, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -5.38]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -56.76]) 
        cylinder(h=100, d=6.8, $fn=53, center=true);
      }
      rotate(a=[0, 0, 135]) 
      translate(v=[21.9486, 0, 0]) 
      rotate(a=[180, 0, 0]) 
      union() {
        // thread
        cylinder(h=8, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -5.38]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -56.76]) 
        cylinder(h=100, d=6.8, $fn=53, center=true);
      }
      rotate(a=[0, 0, 225]) 
      translate(v=[21.9486, 0, 0]) 
      rotate(a=[180, 0, 0]) 
      union() {
        // thread
        cylinder(h=8, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -5.38]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -56.76]) 
        cylinder(h=100, d=6.8, $fn=53, center=true);
      }
      rotate(a=[0, 0, 315]) 
      translate(v=[21.9486, 0, 0]) 
      rotate(a=[180, 0, 0]) 
      union() {
        // thread
        cylinder(h=8, d=3.38, $fn=26, center=true);
        // head
        translate(v=[0, 0, -5.38]) 
        cylinder(h=2.8, d=6.8, $fn=53, center=true);
        // head_clearance
        translate(v=[0, 0, -56.76]) 
        cylinder(h=100, d=6.8, $fn=53, center=true);
      }
    }
    // central_bulge
    translate(v=[0.0, 0.0, 43.0]) 
    cylinder(h=4, d=22.4, $fn=175, center=true);
    // shaft
    translate(v=[0.0, 0.0, 55.48]) 
    cylinder(h=27, d=5.4, $fn=42, center=true);
  }
  // bearing
  translate(v=[-22.0, 0.0, 4.7]) 
  difference() {
    // outer
    cylinder(h=5.4, d=14.4, $fn=113, center=true);
    // inner
    cylinder(h=5.44, d=5.4, $fn=42, center=true);
  }
  // side_bolt_front
  translate(v=[1.19, 36.15, -15.1]) 
  rotate(a=[0, 90, 0]) 
  union() {
    hull() 
    {
      // thread
      cylinder(h=10, d=6.374, $fn=50, center=true);
      translate(v=[0, 20, 0]) 
      // thread
      cylinder(h=10, d=6.374, $fn=50, center=true);
    }
    hull() 
    {
      translate(v=[0, 0, -8.18]) 
      cylinder(h=6.4, d=11.9, $fn=93, center=true);
      translate(v=[0, 20, -8.18]) 
      cylinder(h=6.4, d=11.9, $fn=93, center=true);
    }
    hull() 
    {
      translate(v=[0, 0, -61.36]) 
      cylinder(h=100, d=11.9, $fn=93, center=true);
      translate(v=[0, 20, -61.36]) 
      cylinder(h=100, d=11.9, $fn=93, center=true);
    }
  }
  // side_bolt_back
  translate(v=[1.19, -36.15, -15.1]) 
  rotate(a=[0, 90, 0]) 
  union() {
    hull() 
    {
      // thread
      cylinder(h=10, d=6.374, $fn=50, center=true);
      translate(v=[0, -20, 0]) 
      // thread
      cylinder(h=10, d=6.374, $fn=50, center=true);
    }
    hull() 
    {
      translate(v=[0, 0, -8.18]) 
      cylinder(h=6.4, d=11.9, $fn=93, center=true);
      translate(v=[0, -20, -8.18]) 
      cylinder(h=6.4, d=11.9, $fn=93, center=true);
    }
    hull() 
    {
      translate(v=[0, 0, -61.36]) 
      cylinder(h=100, d=11.9, $fn=93, center=true);
      translate(v=[0, -20, -61.36]) 
      cylinder(h=100, d=11.9, $fn=93, center=true);
    }
  }
  // top_bolt_front
  translate(v=[15.1, 33.15, -1.19]) 
  rotate(a=[180, 0, 0]) 
  union() {
    // thread
    cylinder(h=10, d=6.374, $fn=50, center=true);
    // head
    translate(v=[0, 0, -8.18]) 
    cylinder(h=6.4, d=11.9, $fn=93, center=true);
    // head_clearance
    translate(v=[0, 0, -61.36]) 
    cylinder(h=100, d=11.9, $fn=93, center=true);
  }
  // top_bolt_center
  translate(v=[15.1, 0.0, -1.19]) 
  rotate(a=[180, 0, 0]) 
  union() {
    // thread
    cylinder(h=10, d=6.374, $fn=50, center=true);
    // head
    translate(v=[0, 0, -8.18]) 
    cylinder(h=6.4, d=11.9, $fn=93, center=true);
    // head_clearance
    translate(v=[0, 0, -61.36]) 
    cylinder(h=100, d=11.9, $fn=93, center=true);
  }
  // top_bolt_back
  translate(v=[15.1, -33.15, -1.19]) 
  rotate(a=[180, 0, 0]) 
  union() {
    // thread
    cylinder(h=10, d=6.374, $fn=50, center=true);
    // head
    translate(v=[0, 0, -8.18]) 
    cylinder(h=6.4, d=11.9, $fn=93, center=true);
    // head_clearance
    translate(v=[0, 0, -61.36]) 
    cylinder(h=100, d=11.9, $fn=93, center=true);
  }
  // stepper_clearance
  difference() {
    // volume
    translate(v=[-23.075, 0.0, -18.6]) 
    cube(size=[48.15, 50.3, 35.2], center=true);
    // front_top_chamfer
    translate(v=[-47.17, 21.15, -5.0]) 
    rotate(a=[90, 0, 0]) 
    rotate(a=[0, 90, 0]) 
    linear_extrude(height=48.19, center=false, convexity=10, twist=0.0, scale=1.0) 
    difference() {
      // box
      translate(v=[2.0, 2.0, 0.0]) 
      square(size=[4.04, 4.04], center=true);
      // fillet
      circle(d=8, $fn=62);
    }
    // back_top_chamfer
    translate(v=[-47.17, -21.15, -5.0]) 
    rotate(a=[180, 0, 0]) 
    rotate(a=[0, 90, 0]) 
    linear_extrude(height=48.19, center=false, convexity=10, twist=0.0, scale=1.0) 
    difference() {
      // box
      translate(v=[2.0, 2.0, 0.0]) 
      square(size=[4.04, 4.04], center=true);
      // fillet
      circle(d=8, $fn=62);
    }
  }
  // tilted_clearance
  rotate(a=[0, 35, 0]) 
  translate(v=[-14.075, 0.0, -42.6]) 
  cube(size=[48.15, 58.34, 33.2], center=true);
}