from muscad import Part, TT, Circle, Square, Volume, Surface
from muscad.vitamins.bolts import Bolt
from muscad.vitamins.brackets import CastBracket
from muscad.vitamins.extrusions import Extrusion


class Extrusion3030Insert(Part):
    def init(self, length=50):
        self.body = Volume(width=8 - TT, depth=length, height=6).fillet_depth(
            0.5, bottom=True
        )
        self.wings = Surface.free(
            Circle(d=1.5, segments=20).align(center_x=6, back=-0.5),
            Circle(d=1.5, segments=20).align(center_x=-6, back=-0.5),
            Square(width=8, depth=1).align(center_x=0, front=3),
        ).y_linear_extrude(length)


class Extrusion3030Endcap(Part):
    base = Volume(width=30, depth=30, height=6).fillet_height(4)
    x_insert = (
        Extrusion3030Insert(25)
        .front_to_right()
        .align(left=base.right, center_y=base.center_y, bottom=base.bottom)
    )
    y_insert = Extrusion3030Insert(25).align(
        center_x=base.center_x, back=base.front, bottom=base.bottom
    )
    x_insert_left = (
        Extrusion3030Insert(25)
        .back_to_top()
        .back_to_left()
        .align(left=base.left, center_y=base.center_y, bottom=base.top)
    )
    z_insert_back = (
        Extrusion3030Insert(25)
        .bottom_to_back()
        .align(center_x=base.center_x, back=base.back, bottom=base.top)
    )


class Extrusion3030CastHidder(Part):
    z_extrusion = ~Extrusion.e3030(60).background()
    y_extrusion = (
        ~Extrusion.e3030(60)
        .bottom_to_front()
        .align(
            center_x=z_extrusion.center_x,
            back=z_extrusion.front,
            bottom=z_extrusion.bottom,
        )
        .background()
    )
    x_extrusion = (
        ~Extrusion.e3030(60)
        .bottom_to_right()
        .align(
            left=z_extrusion.right,
            center_y=z_extrusion.center_y,
            bottom=z_extrusion.bottom,
        )
        .background()
    )

    cast_bracket = (
        ~CastBracket.bracket3030()
        .align(
            left=y_extrusion.right,
            back=x_extrusion.front,
            center_z=x_extrusion.center_z,
        )
        .background()
    )
    base = Surface.free(
        Circle(d=4).align(left=z_extrusion.left, back=z_extrusion.back),
        Circle(d=10).align(
            left=z_extrusion.left, front=cast_bracket.front + 2
        ),
        Circle(d=10).align(
            right=cast_bracket.right + 2, back=z_extrusion.back
        ),
        Circle(d=20).align(
            right=cast_bracket.right + 2, center_y=x_extrusion.front
        ),
        Circle(d=20).align(
            center_x=y_extrusion.right, front=cast_bracket.front + 2
        ),
    ).z_linear_extrude(6, top=z_extrusion.bottom)

    x_insert_left = (
        Extrusion3030Insert(30)
        .back_to_top()
        .back_to_left()
        .align(left=base.left, center_y=z_extrusion.center_y, bottom=base.top)
    )
    z_insert_back = (
        Extrusion3030Insert(30)
        .bottom_to_back()
        .align(center_x=z_extrusion.center_x, back=base.back, bottom=base.top)
    )

    right_bolt = ~Bolt.M5(10).align(
        center_x=base.right - 10,
        center_y=x_extrusion.center_y,
        center_z=x_extrusion.bottom - 2,
    )

    front_bolt = ~Bolt.M5(10).align(
        center_x=y_extrusion.center_x,
        center_y=base.front - 10,
        center_z=y_extrusion.bottom - 2,
    )


if __name__ == "__main__":
    Extrusion3030Endcap().render_to_file()
    Extrusion3030CastHidder().render_to_file()
