from muscad import Part, Volume, Object, MirroredPart, E, EE, TT
from muscad.vitamins.boards import Board
from muscad.vitamins.bolts import Bolt
from muscad.vitamins.extrusions import Extrusion


class BoardHolderSide(Part):
    z_extrusion = ~Extrusion.e3030(150).debug()

    def init(self, board, side="right", bolt_spacing=15):
        if side == "left":
            self.board = (
                ~board.top_to_right()
                .align(
                    left=self.z_extrusion.left + 15,
                    back=self.z_extrusion.front + 10,
                    center_z=self.z_extrusion.center_z,
                )
                .debug()
            )
            self.holder = (
                Volume(
                    left=self.z_extrusion.left + 2,
                    width=6,
                    back=self.z_extrusion.front,
                    front=self.board.front + 2,
                    top=self.board.top + 2,
                    bottom=self.board.bottom - 2,
                ).fillet_width()
                - Volume(
                    left=self.z_extrusion.left,
                    width=10,
                    back=self.z_extrusion.front + 20,
                    front=self.board.front - 10,
                    top=self.board.top - 10,
                    bottom=self.board.bottom + 10,
                ).fillet_width()
            )
            self.z_bolt_top = (
                ~Bolt.M6(10)
                .bottom_to_front()
                .align(
                    center_x=self.z_extrusion.center_x,
                    center_y=self.z_extrusion.front,
                    center_z=self.holder.top + 10,
                )
            )

            self.z_bolt_bottom = (
                ~Bolt.M6(10)
                .bottom_to_front()
                .align(
                    center_x=self.z_extrusion.center_x,
                    center_y=self.z_extrusion.front,
                    center_z=self.holder.bottom - 10,
                )
            )

            self.z_attach = Volume(
                center_x=self.z_extrusion.center_x,
                width=26,
                back=self.z_extrusion.front,
                depth=6,
                bottom=self.z_bolt_bottom.bottom - 5,
                top=self.z_bolt_top.top + 5,
            ).fillet_depth(13)

        elif side == "right":
            self.board = (
                ~board.top_to_right()
                .align(
                    left=self.z_extrusion.left + 15,
                    front=self.z_extrusion.back - 10,
                    center_z=self.z_extrusion.center_z,
                )
                .debug()
            )
            self.holder = (
                Volume(
                    left=self.z_extrusion.left + 2,
                    width=6,
                    front=self.z_extrusion.back,
                    back=self.board.back - 2,
                    top=self.board.top + 2,
                    bottom=self.board.bottom - 2,
                ).fillet_width()
                - Volume(
                    left=self.z_extrusion.left,
                    width=10,
                    front=self.z_extrusion.back - 20,
                    back=self.board.back + 10,
                    top=self.board.top - 10,
                    bottom=self.board.bottom + 10,
                ).fillet_width()
            )

            self.z_bolt_top = (
                ~Bolt.M6(10)
                .bottom_to_back()
                .align(
                    center_x=self.z_extrusion.center_x,
                    center_y=self.z_extrusion.back,
                    center_z=self.holder.top + bolt_spacing,
                )
            )

            self.z_bolt_bottom = (
                ~Bolt.M6(10)
                .bottom_to_back()
                .align(
                    center_x=self.z_extrusion.center_x,
                    center_y=self.z_extrusion.back,
                    center_z=self.holder.bottom - bolt_spacing,
                )
            )

            self.z_attach = Volume(
                center_x=self.z_extrusion.center_x,
                width=26,
                front=self.z_extrusion.back,
                depth=6,
                bottom=self.z_bolt_bottom.bottom - 5,
                top=self.z_bolt_top.top + 5,
            ).fillet_depth(13)


class BoardHolderCorner(Part):
    z_extrusion = ~Extrusion.e3030(200).debug()
    y_extrusion = (
        ~Extrusion.e3030(200)
        .bottom_to_front()
        .align(
            right=z_extrusion.right,
            back=z_extrusion.front,
            bottom=z_extrusion.bottom,
        )
        .debug()
    )

    def init(
        self,
        board: Object,
        hollow_offset_bottom=10,
        hollow_offset_side=10,
        brand: str = None,
    ):

        self.board = (
            ~board.bottom_to_left()
            .align(
                left=self.y_extrusion.center_x - 5,
                back=self.z_extrusion.front + 10,
                bottom=self.y_extrusion.top + 10,
            )
            .debug()
        )

        self.holder = (
            Volume(
                left=self.z_extrusion.left + 2,
                width=6,
                back=self.z_extrusion.front,
                front=self.board.front + 2,
                top=self.board.top + 2,
                bottom=self.y_extrusion.top,
            ).fillet_width()
            - Volume(
                left=self.z_extrusion.left,
                width=10,
                back=self.z_extrusion.front + hollow_offset_side,
                front=self.board.front - 10,
                top=self.board.top - 10,
                bottom=self.y_extrusion.top + hollow_offset_bottom,
            ).fillet_width()
        )

        self.z_bolt = (
            ~Bolt.M6(10)
            .bottom_to_front()
            .align(
                center_x=self.z_extrusion.center_x,
                center_y=self.z_extrusion.front,
                center_z=self.holder.top + 10,
            )
        )
        self.z_attach = (
            Volume(
                center_x=self.z_extrusion.center_x,
                width=26,
                back=self.z_extrusion.front,
                depth=6,
                bottom=self.y_extrusion.top,
                top=self.z_bolt.top + 5,
            )
            .fillet_depth(13, top=True)
            .fillet_width(back=True, bottom=True)
        )

        self.y_bolt = (
            ~Bolt.M6(10)
            .upside_down()
            .align(
                center_x=self.y_extrusion.center_x,
                center_y=self.holder.front + 10,
                center_z=self.y_extrusion.top,
            )
        )
        self.y_attach = (
            Volume(
                center_x=self.z_extrusion.center_x,
                width=26,
                back=self.z_extrusion.front,
                front=self.y_bolt.front + 5,
                bottom=self.y_extrusion.top,
                height=6,
            )
            .fillet_height(13, front=True)
            .fillet_width(back=True, bottom=True)
        )


class PowerSupplyHolder(BoardHolderCorner):
    def init(self):
        power_supply = Board.smps300rs(Bolt.M3(20).add_nut(-1).upside_down())
        return super().init(power_supply, hollow_offset_bottom=20)


class ScreenHolder(BoardHolderSide):
    def init(self):
        screen = Board.lcd12864(
            Bolt.M3(25).add_nut(-1).upside_down().down(7.5)
        )
        return super().init(screen, side="right")


class MainboardHolder(BoardHolderCorner):
    def init(self):
        mainboard = Board.mks_sbase(Bolt.M3(20).add_nut(-1).upside_down())
        return super().init(mainboard, hollow_offset_side=20)


class ScreenCover(Part):
    _holder = ScreenHolder()
    screen = ~_holder.board.screen
    sd = ~_holder.board.sd_card
    bolts = ~_holder.board.bolts
    cover = (
        Volume(
            right=_holder.right - EE,
            left=_holder.left + E,
            front=_holder.z_attach.back - TT,
            back=_holder.back - 4,
            top=_holder.holder.top + 4,
            bottom=_holder.holder.bottom - 4,
        )
        .fillet_width(r=4, back=True)
        .fillet_depth(r=4, right=True)
        .fillet_height(r=4, right=True, back=True)
        - Volume(
            right=_holder.right - 7,
            left=_holder.left - E,
            front=_holder.front + TT,
            back=_holder.back,
            top=_holder.holder.top,
            bottom=_holder.holder.bottom,
        ).fillet_width(r=4)
    )


if __name__ == "__main__":
    ScreenHolder().render_to_file()
    PowerSupplyHolder().render_to_file()
    MainboardHolder().render_to_file()
    ScreenCover().render_to_file()
